
-- Copyright (C) 2020-2021 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

_G.DSprays = _G.DSprays or {}

DSprays.Threads = DSprays.Threads or {}
DSprays.IThreads = DSprays.IThreads or {}

local function Think()
	local thread = DSprays.Threads[1]
	if not thread then return end

	local status, errors = coroutine.resume(thread)

	if coroutine.status(thread) == 'dead' then
		table.remove(DSprays.Threads, 1)
	end

	if not status then
		error(errors)
	end
end

local function Think2()
	if CLIENT then
		local time = RealTimeL()

		for i = #DSprays.CanDummies, 1, -1 do
			local can = DSprays.CanDummies[i]

			if can:IsValid() then
				if can.last_use < time then
					can:Remove()
					table.remove(DSprays.CanDummies, i)
				end
			else
				table.remove(DSprays.CanDummies, i)
			end
		end
	end

	local num = 0

	for id, thread in pairs(DSprays.IThreads) do
		local status, errors = coroutine.resume(thread)

		if coroutine.status(thread) == 'dead' then
			DSprays.IThreads[id] = nil
		end

		if not status then
			error(errors)
		end

		num = num + 1

		if num > 3 then break end
	end
end

function DSprays.PushThread(thread)
	if table.qhasValue(DSprays.Threads, thread) then
		return false
	end

	table.insert(DSprays.Threads, thread)
	return true
end

function DSprays.PushIdentifiedThread(name, thread)
	DSprays.IThreads[name] = thread
end

hook.Add('Think', 'DSprays Computing Thread', Think)
hook.Add('Think', 'DSprays Computing Thread 2', Think2)

DSprays.RatingList = {
	'gui.dsrpays.rating.safe',
	'gui.dsrpays.rating.suggestive',
	'gui.dsrpays.rating.questionable',
	'gui.dsrpays.rating.explicit',
}

DSprays.TYPE_STATIC_PICTURE = 1
DSprays.TYPE_DYNAMIC_PICTURE = 2
DSprays.TYPE_DYNAMIC_VIDEO = 3

CAMI.RegisterPrivilege({
	Name = 'dsprays_admin',
	MinAccess = 'admin',
	Description = 'gui.cami.dsprays.dsprays_admin'
})

if CLIENT then
	DSprays.camiwatchdog = DLib.CAMIWatchdog('dsprays_admin', 10, 'dsprays_admin')
end

DSprays.SprayTypeName = {
	[DSprays.TYPE_STATIC_PICTURE] = 'gui.dsrpays.main.types.static',
	[DSprays.TYPE_DYNAMIC_PICTURE] = 'gui.dsrpays.main.types.dynamic',
	[DSprays.TYPE_DYNAMIC_VIDEO] = 'gui.dsrpays.main.types.dynamic_video',
}

function DSprays.ValidURL(url)
	return isstring(url) and url:match('^https?://')
end

function DSprays.TranslateURL(url)
	if pac and pac.FixURL then
		return pac.FixURL(url)
	end

	url = url:Trim()

	if url:find("dropbox", 1, true) then
		url = url:gsub([[^http%://dl%.dropboxusercontent%.com/]], [[https://dl.dropboxusercontent.com/]])
		url = url:gsub([[^https?://dl.dropbox.com/]], [[https://www.dropbox.com/]])
		url = url:gsub([[^https?://www.dropbox.com/s/(.+)%?dl%=[01]$]], [[https://dl.dropboxusercontent.com/s/%1]])
		url = url:gsub([[^https?://www.dropbox.com/s/(.+)$]], [[https://dl.dropboxusercontent.com/s/%1]])
		return url
	end

	if url:find("drive.google.com", 1, true) and not url:find("export=download", 1, true) then
		local id =
			url:match("https://drive.google.com/file/d/(.-)/") or
			url:match("https://drive.google.com/file/d/(.-)$") or
			url:match("https://drive.google.com/open%?id=(.-)$")

		if id then
			return "https://drive.google.com/uc?export=download&id=" .. id
		end
		return url
	end

	url = url:gsub([[^http%://onedrive%.live%.com/redir?]],[[https://onedrive.live.com/download?]])

	return url
end

DLib.CMessageChat(DSprays, 'DSprays')

if CLIENT then
	include('dsprays/sh_pointmap.lua')
	include('dsprays/cl_handler.lua')
	include('dsprays/cl_render.lua')
	include('dsprays/cl_data.lua')
	include('dsprays/cl_texture.lua')
	include('dsprays/cl_panels.lua')
	include('dsprays/cl_logic.lua')
	include('dsprays/cl_object.lua')
	include('dsprays/cl_contextmenu.lua')
else
	AddCSLuaFile('dsprays/sh_pointmap.lua')
	AddCSLuaFile('dsprays/cl_data.lua')
	AddCSLuaFile('dsprays/cl_texture.lua')
	AddCSLuaFile('dsprays/cl_handler.lua')
	AddCSLuaFile('dsprays/cl_render.lua')
	AddCSLuaFile('dsprays/cl_panels.lua')
	AddCSLuaFile('dsprays/cl_logic.lua')
	AddCSLuaFile('dsprays/cl_object.lua')
	AddCSLuaFile('dsprays/cl_contextmenu.lua')
	include('dsprays/sh_pointmap.lua')
	include('dsprays/sv_logic.lua')
end
