
-- Copyright (C) 2020-2021 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

include('shared.lua')

local dsprays_animated = CreateConVar('dsprays_animated', '1', {FCVAR_ARCHIVE}, 'No not play animations by default')

cvars.AddChangeCallback('dsprays_animated', function()
	hook.Run('DSprays_SpraySettingsChanged', dsprays_animated:GetBool())
end, 'DSprays')

DSprays.dsprays_animated = dsprays_animated

AccessorFunc(ENT, 'spray_html_panel', 'HTMLPanel')
AccessorFunc(ENT, 'spray_html_data', 'HTMLPanelData')
AccessorFunc(ENT, 'do_not_animate', 'DoNotAnimate')

function ENT:Initialize()
	hook.Add('DSprays_SpraySettingsChanged', self, self.MarkDirty)
end

local dsprays_fullbright = DSprays.dsprays_fullbright
local PushFilterMag = render.PushFilterMag
local PushFilterMin = render.PushFilterMin
local PopFilterMin = render.PopFilterMin
local PopFilterMag = render.PopFilterMag
local IgnoreZ = cam.IgnoreZ
local SuppressEngineLighting = render.SuppressEngineLighting
local ANISOTROPIC = TEXFILTER.ANISOTROPIC

function ENT:GetRealDoNotAnimate()
	if self.do_not_animate == nil then return not dsprays_animated:GetBool() end
	return self.do_not_animate
end

function ENT:MarkDirty()
	self._dirty_rev = nil

	if self:GetNoDraw() then
		self:Rebuild()
	end
end

function ENT:CheckDirty()
	if self._dirty_rev == self:GetRev() then return false end

	if self._dirty_rev then
		if not self._dirty_rebuild then
			self._dirty_rebuild = SysTime() + 0.5
		end

		if self._dirty_rebuild < SysTime() then
			self:Rebuild()
			hook.Add('DSprays_SpraySettingsChanged', self, self.MarkDirty)
			self._dirty_rebuild = nil
		end
	else
		self:Rebuild()
		hook.Add('DSprays_SpraySettingsChanged', self, self.MarkDirty)
	end

	return true
end

function ENT:OnRemove()
	if IsValid(self.spray_object) then
		self.spray_object:Remove()
	end

	-- Left PVS and full update - clean up
	-- so if we are not actually removed
	-- we can be alive again
	self:MarkDirty()
end

function ENT:Draw(hl)
	self:CheckDirty()

	if not IsValid(self.spray_object) then return end

	PushFilterMag(ANISOTROPIC)
	PushFilterMin(ANISOTROPIC)

	local fb = dsprays_fullbright:GetBool() or hl

	if fb then
		SuppressEngineLighting(true)
	end

	if hl then
		IgnoreZ(true)
	end

	while self.spray_object:DoRenderEngineLayered() do
		self:DrawModel()
	end

	if hl then
		IgnoreZ(false)
	end

	if fb then
		SuppressEngineLighting(false)
	end

	PopFilterMin()
	PopFilterMag()
end

-- https://github.com/Facepunch/garrysmod-issues/issues/3029
function ENT:GetRenderMesh()
	return self.spray_object:GetRenderMeshCurrentLayered()
end

function ENT:LoadURL(url, rating, obj)
	obj = obj or self.spray_object

	if self:IsDynamicVideo() and not self:GetRealDoNotAnimate() then
		if DSprays.IsRatingCorrect(rating) then
			DSprays.VideoURLTexture(DSprays.TranslateURL(url), DSprays.GetDefaultSizeVideo(), DSprays.GetDefaultSizeVideo()):Then(function(data)
				if not IsValid(self) or not IsValid(obj) then return end
				obj:SetMaterial(data.material)
				self:SetHTMLPanel(data.panel)
				self:SetHTMLPanelData(data)
				obj:SetRenderFeedback(data)
			end)
		else
			DSprays.VideoAsStaticURLTexture(DSprays.TranslateURL(url), DSprays.GetCensorSize(), DSprays.GetCensorSize()):Then(function(data)
				if not IsValid(self) or not IsValid(obj) then return end
				obj:SetMaterial(data.material)
				self:SetHTMLPanel()
				self:SetHTMLPanelData()
				obj:SetRenderFeedback()
			end)
		end
	elseif self:IsDynamicVideo() and self:GetRealDoNotAnimate() then
		if DSprays.IsRatingCorrect(rating) then
			DSprays.VideoAsStaticURLTexture(DSprays.TranslateURL(url), DSprays.GetDefaultSize(), DSprays.GetDefaultSize()):Then(function(data)
				if not IsValid(self) or not IsValid(obj) then return end
				obj:SetMaterial(data.material)
				self:SetHTMLPanel()
				self:SetHTMLPanelData()
				obj:SetRenderFeedback()
			end)
		else
			DSprays.VideoAsStaticURLTexture(DSprays.TranslateURL(url), DSprays.GetCensorSize(), DSprays.GetCensorSize()):Then(function(data)
				if not IsValid(self) or not IsValid(obj) then return end
				obj:SetMaterial(data.material)
				self:SetHTMLPanel()
				self:SetHTMLPanelData()
				obj:SetRenderFeedback()
			end)
		end
	elseif self:IsDynamicPicture() and not self:GetRealDoNotAnimate() then
		if DSprays.IsRatingCorrect(rating) then
			DSprays.DynamicURLTexture(DSprays.TranslateURL(url), DSprays.GetDefaultSizeVideo(), DSprays.GetDefaultSizeVideo()):Then(function(data)
				if not IsValid(self) or not IsValid(obj) then return end
				obj:SetMaterial(data.material)
				obj:SetRenderFeedback(data)
				self:SetHTMLPanel()
				self:SetHTMLPanelData()
			end)
		else
			DSprays.StaticURLTexture(DSprays.TranslateURL(url), DSprays.GetCensorSize(), DSprays.GetCensorSize()):Then(function(data)
				if not IsValid(self) or not IsValid(obj) then return end
				obj:SetMaterial(data.material)
				obj:SetRenderFeedback()
				self:SetHTMLPanel()
				self:SetHTMLPanelData()
			end)
		end
	else
		if DSprays.IsRatingCorrect(rating) then
			DSprays.StaticURLTexture(DSprays.TranslateURL(url), DSprays.GetDefaultSize(), DSprays.GetDefaultSize()):Then(function(data)
				if not IsValid(self) or not IsValid(obj) then return end
				obj:SetMaterial(data.material)
				obj:SetRenderFeedback()
				self:SetHTMLPanel()
				self:SetHTMLPanelData()
			end)
		else
			DSprays.StaticURLTexture(DSprays.TranslateURL(url), DSprays.GetCensorSize(), DSprays.GetCensorSize()):Then(function(data)
				if not IsValid(self) or not IsValid(obj) then return end
				obj:SetMaterial(data.material)
				obj:SetRenderFeedback()
				self:SetHTMLPanel()
				self:SetHTMLPanelData()
			end)
		end
	end
end

function ENT:DetermineHide(rating, hide_url, hide_author, obj)
	obj = obj or self.spray_object

	local hide = DSprays.dsprays_hide_everything:GetBool()

	if DSprays.IsRatingCorrect(rating) then
		obj:PopText('nsfw')
	else
		obj:PushText('nsfw', DLib.I18n.Localize(DSprays.RatingList[rating] or 'gui.dsrpays.rating.nsfw'))

		if DSprays.dsprays_hide_unmatching:GetBool() then
			hide = true
		end
	end

	if hide_author then
		obj:PopText('url_hidden')

		local sha = DLib.Util.QuickSHA1(self:GetSprayURL())
		obj:PushText('author_hidden', DLib.I18n.Localize('gui.dsrpays.misc.author_hidden') .. '\n' .. sha:sub(1, 20) .. '\n' .. sha:sub(21))

		if DSprays.dsprays_hide_unmatching:GetBool() then
			hide = true
		end
	elseif hide_url then
		obj:PopText('author_hidden')

		local sha = DLib.Util.QuickSHA1(self:GetSprayURL())
		obj:PushText('url_hidden', DLib.I18n.Localize('gui.dsrpays.misc.url_hidden') .. '\n' .. sha:sub(1, 20) .. '\n' .. sha:sub(21))

		if DSprays.dsprays_hide_unmatching:GetBool() then
			hide = true
		end
	else
		obj:PopText('author_hidden')
		obj:PopText('url_hidden')
	end

	if hide_url or hide_author then
		if self:GetSprayLabel():trim() ~= '' then
			obj:PushText('!sprayed_by', DLib.I18n.Localize('gui.dsrpays.controls.label') .. ' ' .. self:GetSprayLabel())
		end

		if self:GetSpraySteamID() ~= '' then
			obj:PushText('!sprayed_by2', DLib.I18n.Localize('gui.dsrpays.controls.sprayed_by') .. ' ' .. DLib.LastNickFormatted(self:GetSpraySteamID()) .. '\n' .. self:GetSpraySteamID())
		end
	else
		obj:PopText('!sprayed_by')
		obj:PopText('!sprayed_by2')
	end

	return hide
end

