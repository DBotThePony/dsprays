
-- Copyright (C) 2020-2021 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

include('shared.lua')

DEFINE_BASECLASS(ENT.Base)

function ENT:Initialize()
	self:SetSolid(SOLID_NONE)
	self:DrawShadow(false)

	BaseClass.Initialize(self)
end

function ENT:OnRemove()
	BaseClass.OnRemove(self)

	if IsValid(self._mesh) then
		self._mesh:Destroy()
	end

	self._dirty_pos = nil
	self._dirty_ang = nil
end

local dsprays_fullbright = DSprays.dsprays_fullbright
local PushFilterMag = render.PushFilterMag
local PushFilterMin = render.PushFilterMin
local PopFilterMin = render.PopFilterMin
local PopFilterMag = render.PopFilterMag
local IgnoreZ = cam.IgnoreZ
local SuppressEngineLighting = render.SuppressEngineLighting
local ANISOTROPIC = TEXFILTER.ANISOTROPIC

local DSprays = DSprays

function ENT:Draw()
	local hl = IsValid(DSprays.highlight_panel)
	if hl and self.highlight_panel ~= DSprays.highlight_panel then return end

	return BaseClass.Draw(self, hl)
end

local cl_logofile = GetConVar("cl_logofile")

local color_box = Color(86, 255, 114, 50)
local absolute_mins = Vector(-0xFFFF, -0xFFFF, -0xFFFF)
local absolute_maxs = Vector(0xFFFF, 0xFFFF, 0xFFFF)
local vector_up = Vector(0, 0, 1)

function ENT:Rebuild()
	if self:GetSpraySteamID() == '' then return false end
	if not DSprays.ValidURL(self:GetSprayURL()) and not self:GetSourceSprayPlayer() then return false end

	if not IsValid(self.spray_object) then
		self.spray_object = DSprays.MeshObject(self:GetPos(), self:GetAngles())
	end

	self._dirty_rev = self:GetRev()

	local obj = self.spray_object
	local rating = self:GetSprayRating()
	local hide_url = DSprays.IsURLHidden(self:GetSprayURL())
	local hide_author = self:GetSpraySteamID() ~= '' and DSprays.IsSteamIDHidden(self:GetSpraySteamID())

	local hide = self:DetermineHide(rating, hide_url, hide_author, obj)

	if hide then
		self:SetNoDraw(true)
		return
	end

	obj:SetBackgroundColor(not hide_url and self:GetSprayBackgroundC() or nil)
	self:SetNoDraw(false)

	if self._dirty_pos ~= self:GetPos() or self._dirty_ang ~= self:GetAngles() then
		self._dirty_pos = self:GetPos()
		self._dirty_ang = self:GetAngles()
		local spos = self:GetPos()
		local sang = self:GetAngles()

		local function builder()
			for i, quality in ipairs(self.MESH_MAP_QUALITIES) do
				if not IsValid(self) then return end

				local map = DSprays.PointMap(quality, quality, self:GetSpraySize(), nil, spos, sang, self, self:GetParent())
				local point = DSprays.Point(map, Vector(), Angle(), 0, 0)

				while #map.open ~= 0 do
					map:Close()
				end

				map:Calculate()

				local vertex = map:BuildMesh()
				if not IsValid(self) then return end
				local mins, maxs, minsL, maxsL = absolute_maxs, absolute_mins, 0xFFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFF
				local minsA, maxsA = Angle(), Angle()

				for i = 3, #vertex do
					local data = vertex[i]
					local dot = data.normal:Dot(vector_up)
					--local lpos, lang = WorldToLocal(data.pos, dot ~= 1 and dot ~= -1 and data.normal:Angle() or sang, spos, sang)
					local lpos, lang = data.pos, data.normal:Angle()
					local lenL, lenl = lpos:DistToSqr(absolute_maxs), lpos:DistToSqr(absolute_mins)

					if minsL > lenl then
						mins = lpos
						minsA = lang
						minsL = lenl
					end

					if maxsL > lenL then
						maxs = lpos
						maxsA = lang
						maxsL = lenL
					end
				end

				if not IsValid(self) then return end

				if math.abs(mins.x - maxs.x) < 1 then
					local xX = (mins.x + maxs.x) / 2
					mins.x = xX - mins.x / 2
					maxs.x = xX + maxs.x / 2
				end

				if math.abs(mins.y - maxs.y) < 1 then
					local yX = (mins.y + maxs.y) / 2
					mins.y = yX - mins.y / 2
					maxs.y = yX + maxs.y / 2
				end

				if math.abs(mins.z - maxs.z) < 1 then
					local zX = (mins.z + maxs.z) / 2
					mins.z = zX - mins.z / 2
					maxs.z = zX + maxs.z / 2
				end

				self._calc_mins = mins
				self._calc_maxs = maxs

				self:SetRenderBounds(mins, maxs)

				debugoverlay.BoxAngles(spos, mins, maxs, sang, 4, color_box)

				if self.spray_object then
					local mesh = Mesh()
					mesh:BuildFromTriangles(vertex)

					self.spray_object:SetIMesh(mesh)
					self._mesh = mesh
				end
			end
		end

		local thread = coroutine.create(builder)
		coroutine.resume(thread)
		DSprays.PushIdentifiedThread(self:GetSprayIndex(), thread)
	end

	-- obj:SetIgnoreLight(DSprays.dsprays_fullbright:GetBool())

	if self:GetSourceSprayPlayer() and not hide_author then
		local ply = game.SinglePlayer() and Entity(1) or player.GetBySteamID(self:GetSpraySteamID())

		if ply then
			local path, hash, path_new, path_new2

			if game.SinglePlayer() then
				path_new2 = cl_logofile:GetString():gsub('%.vtf', ''):gsub('materials/', '')
			else
				local info = ply:GetPlayerInfo()

				if not istable(info) then
					obj:SetMaterial(DSprays.SourceEngineSprayMissing)
					return
				end

				if not istable(info.customfiles) then
					obj:SetMaterial(DSprays.SourceEngineSprayMissing)
					return
				end

				if not isstring(info.customfiles[1]) then
					obj:SetMaterial(DSprays.SourceEngineSprayMissing)
					return
				end

				path = 'download/user_custom/' .. info.customfiles[1]:sub(1, 2) .. '/' .. info.customfiles[1] .. '.dat'
				hash = DLib.Util.QuickMD5(path)
				path_new = 'dsprays_cache/' .. hash:sub(1, 2) .. '/' .. hash .. '.vtf'
				path_new2 = '../data/dsprays_cache/' .. hash:sub(1, 2) .. '/' .. hash

				if not file.Exists(path_new, 'DATA') then
					if not file.Exists(path, 'GAME') then
						obj:SetMaterial(DSprays.SourceEngineSprayMissing)
						return
					end

					file.mkdir('dsprays_cache/' .. hash:sub(1, 2))
					file.Write(path_new, file.Read(path, 'GAME'))
				end
			end

			if not ply.dsprays_spray_default_mat then
				ply.dsprays_spray_default_mat = CreateMaterial('dsprays_spray12_' .. ply:SteamID64(), 'VertexLitGeneric', {
					['$basetexture'] = path_new2,
					['$translucent'] = '1',
					['$alpha'] = '1',
					['$nocull'] = 1,
				})
			end

			ply.dsprays_spray_default_mat:SetTexture('$basetexture', path_new2)
			obj:SetMaterial(ply.dsprays_spray_default_mat)
		else
			obj:SetMaterial(DSprays.SourceEngineSprayMissing)
		end
	elseif not hide_url and not hide_author then
		self:LoadURL(self:GetSprayURL(), rating, obj)
	else
		obj:SetMaterial()
	end
end
