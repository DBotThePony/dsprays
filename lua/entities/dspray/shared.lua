
-- Copyright (C) 2020-2021 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

ENT.PrintName = 'Player Spray'
ENT.Base = 'dspray_abstract'
ENT.RenderGroup = RENDERGROUP_TRANSLUCENT
ENT.MESH_MAP_QUALITIES = {1, 4, 16, 32}

DEFINE_BASECLASS(ENT.Base)

function ENT:SetupDataTables()
	BaseClass.SetupDataTables(self)

	self:NetworkVar('Entity', 0, 'SprayParent')
	self:NetworkVar('Bool', 2, 'SourceSprayPlayer')
end

function ENT:GetSprayIndex()
	return self:GetSpraySteamID() .. '_spray'
end

function ENT:CanTool()
	return false
end

function ENT:CanProperty()
	return false
end

local function CanTool(_, tr)
	if IsValid(tr.Entity) and tr.Entity:GetClass() == 'dspray' then return false end
end

local function CanProperty(_, _, ent)
	if IsValid(ent) and ent:GetClass() == 'dspray' then return false end
end

local function CanDrive(_, ent)
	if IsValid(ent) and ent:GetClass() == 'dspray' then return false end
end

local function PhysgunPickup(_, ent)
	if IsValid(ent) and ent:GetClass() == 'dspray' then return false end
end

hook.Add('CanTool', 'DSprays Prevent Entity Change', CanTool)
hook.Add('CanProperty', 'DSprays Prevent Entity Change', CanProperty)
hook.Add('CanDrive', 'DSprays Prevent Entity Change', CanDrive)
hook.Add('PhysgunPickup', 'DSprays Prevent Entity Change', PhysgunPickup)
