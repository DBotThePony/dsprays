
-- Copyright (C) 2020-2021 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

local frame

local function DoPaint(self, w, h)
	if DLib.skin.ENABLE_BLUR:GetBool() then
		DLib.blur.RefreshNow(true)
		DLib.blur.DrawPanel(w, h, self:LocalToScreen(0, 0))
	end

	DLib.skin.tex.Window.Normal(0, 0, w, h)
end

local function ContextMenuOpened()
	if IsValid(frame) then return end

	local ply = LocalPlayer()

	local tr = util.TraceLine({
		start = ply:EyePos(),
		endpos = ply:EyePos() + ply:GetAimVector() * 512,
		filter = ply
	})

	if not tr.Hit then return end

	local spray
	local isEntity = false

	if not IsValid(tr.Entity) or tr.Entity:GetClass() ~= 'dspray_poster' then
		local find = ents.FindInSphere(tr.HitPos, 32)
		local sprays = {}

		for i, ent in ipairs(find) do
			if ent:GetClass() == 'dspray' then
				table.insert(sprays, ent)
			end
		end

		if #sprays == 0 then return end

		table.sort(sprays, function(a, b)
			return b:GetPos():Distance(tr.HitPos) > a:GetPos():Distance(tr.HitPos)
		end)

		spray = sprays[1]
	elseif IsValid(tr.Entity) and tr.Entity:GetClass() == 'dspray_poster' then
		spray = tr.Entity
		isEntity = true
	end

	if IsValid(g_ContextMenu) then
		frame = vgui.Create('EditablePanel', g_ContextMenu)
		frame:SetSkin('DLib_Black')
		frame.Paint = DoPaint
		frame:DockPadding(5, 30, 5, 5)
	else
		frame = vgui.Create('DLib_Window')
		frame:SetTitle('gui.dsrpays.controls.title')
	end

	local _w = IsValid(g_ContextMenu) and g_ContextMenu:GetWide() or ScrW()
	local _h = IsValid(g_ContextMenu) and g_ContextMenu:GetTall() or ScrH()

	frame:SetSize(300, 500)
	frame:SetPos(_w - 320, _h / 2 - 250)

	spray.highlight_panel = frame
	DSprays.highlight_panel = frame

	if spray:GetSprayLabel() ~= '' then
		local label = vgui.Create('DLabel', frame)
		label:Dock(TOP)
		label:DockMargin(5, 5, 5, 5)
		label:SetZPos(-20)
		label:SetText('gui.dsrpays.controls.label')

		label = vgui.Create('DLabel', frame)
		label:Dock(TOP)
		label:DockMargin(20, 0, 5, 5)
		label:SetZPos(-19)
		label:SetText(spray:GetSprayLabel())
	end

	do
		local label = vgui.Create('DLabel', frame)
		label:Dock(TOP)
		label:DockMargin(5, 5, 5, 5)
		label:SetZPos(-10)
		label:SetText('gui.dsrpays.rating.rating')

		label = vgui.Create('DLabel', frame)
		label:Dock(TOP)
		label:DockMargin(20, 0, 5, 5)
		label:SetZPos(-9)
		label:SetText(DSprays.RatingList[spray:GetSprayRating()] or DSprays.RatingList[1])
	end

	do
		local label = vgui.Create('DLabel', frame)

		label:Dock(TOP)
		label:DockMargin(5, 5, 5, 5)
		label:SetZPos(1)
		label:SetText(isEntity and 'gui.dsrpays.controls.created_by' or 'gui.dsrpays.controls.sprayed_by')

		local canvas = vgui.Create('EditablePanel', frame)
		canvas:SetTall(32)
		canvas:Dock(TOP)
		canvas:DockMargin(20, 0, 5, 5)
		canvas:SetZPos(2)

		local steamid = game.SinglePlayer() and ply:SteamID() or spray:GetSpraySteamID()

		local avatar = vgui.Create('DLib_Avatar', canvas)
		avatar:Dock(LEFT)
		avatar:SetWide(32)
		avatar:SetSteamID(steamid)
		avatar:SetZPos(0)

		local nick = vgui.Create('DLabel', canvas)
		nick:Dock(FILL)
		nick:SetZPos(1)
		nick:DockMargin(5, 0, 0, 0)
		nick:SetText(DLib.LastNickFormatted(steamid))

		local hide = vgui.Create('DCheckBoxLabel', frame)
		hide:Dock(TOP)
		hide:DockMargin(20, 0, 5, 5)
		hide:SetZPos(5)
		hide:SetText('gui.dsrpays.controls.hide_author')
		hide.Button:SetChecked(DSprays.IsSteamIDHidden(steamid))

		function hide.Button.OnChange(_, value)
			if value then
				DSprays.HideSteamID(steamid)
			else
				DSprays.ShowSteamID(steamid)
			end

			for i, ent_ in ipairs(ents.FindByClass('dspray')) do
				if ent_:GetSpraySteamID() == steamid then
					ent_:Rebuild()
				end
			end

			for i, ent_ in ipairs(ents.FindByClass('dspray_poster')) do
				if ent_:GetSpraySteamID() == steamid then
					ent_:Rebuild()
				end
			end
		end
	end

	do
		local label = vgui.Create('DLabel', frame)

		label:Dock(TOP)
		label:DockMargin(5, 5, 5, 5)
		label:SetZPos(10)
		label:SetText('gui.dsrpays.main.types.type')

		local label = vgui.Create('DLabel', frame)

		label:Dock(TOP)
		label:DockMargin(20, 0, 5, 5)
		label:SetZPos(11)
		label:SetText(DSprays.SprayTypeName[spray:GetSprayType()] or DSprays.SprayTypeName[1])

		if spray:GetSprayURL() ~= '' then
			local hide = vgui.Create('DCheckBoxLabel', frame)
			hide:Dock(TOP)
			hide:DockMargin(20, 0, 5, 5)
			hide:SetZPos(12)
			hide:SetText('gui.dsrpays.controls.hide_url')
			hide.Button:SetChecked(DSprays.IsURLHidden(spray:GetSprayURL()))

			function hide.Button.OnChange(_, value)
				if value then
					DSprays.HideURL(spray:GetSprayURL())
				else
					DSprays.ShowURL(spray:GetSprayURL())
				end

				local url = spray:GetSprayURL()

				for i, ent_ in ipairs(ents.FindByClass('dspray')) do
					if ent_:GetSprayURL() == url then
						ent_:Rebuild()
					end
				end

				for i, ent_ in ipairs(ents.FindByClass('dspray_poster')) do
					if ent_:GetSprayURL() == url then
						ent_:Rebuild()
					end
				end
			end
		end
	end

	if spray:IsDynamicVideo() and IsValid(spray:GetHTMLPanel()) and spray:GetHTMLPanelData() then
		local label = vgui.Create('DLabel', frame)

		label:Dock(TOP)
		label:DockMargin(5, 5, 5, 5)
		label:SetZPos(20)
		label:SetText('gui.dsrpays.controls.video')

		local panel = spray:GetHTMLPanel()
		local data = spray:GetHTMLPanelData()
		local volume = data.volume or 0

		local pause = vgui.Create('DCheckBoxLabel', frame)
		pause:Dock(TOP)
		pause:DockMargin(20, 0, 5, 5)
		pause:SetZPos(21)
		pause:SetText('gui.dsrpays.controls.video_pause')
		pause.Button:SetChecked(data.force_paused or false)

		function pause.Button.OnChange(_, value)
			data.force_paused = value
		end

		local volume = vgui.Create('DNumSlider', frame)
		volume:Dock(TOP)
		volume:DockMargin(20, 0, 5, 5)
		volume:SetZPos(22)
		volume:SetMin(0)
		volume:SetMax(100)
		volume:SetDecimals(0)
		volume:SetValue(data.volume or 0)
		volume:SetText('gui.dsrpays.controls.volume')

		function volume.OnValueChanged(_, value)
			data.volume = value
		end
	end

	if spray:IsDynamicVideo() or spray:IsDynamicPicture() then
		local pause = vgui.Create('DCheckBoxLabel', frame)
		pause:Dock(TOP)
		pause:DockMargin(20, 0, 5, 5)
		pause:SetZPos(23)
		pause:SetText('gui.dsrpays.controls.animate')
		pause.Button:SetChecked(not spray:GetRealDoNotAnimate())

		function pause.Button.OnChange(_, value)
			spray:SetDoNotAnimate(not value)
			spray:MarkDirty()
		end
	end

	if not isEntity then
		if spray:GetSprayPlayer() == ply then
			local button = vgui.Create('DButton', frame)
			button:Dock(TOP)
			button:DockMargin(5, 5, 5, 5)
			button:SetZPos(30)
			button:SetText('gui.dsrpays.controls.delete')

			function button.DoClick()
				if not IsValid(spray) then return end
				net.Start('dsprays_delete_spray', true)
				net.WriteEntity(spray)
				net.WriteUInt8(0)
				net.SendToServer()
			end
		elseif IsValid(spray:GetSprayParent()) and spray:GetSprayParent().CPPIGetOwner and spray:GetSprayParent():CPPIGetOwner() == ply then
			local button = vgui.Create('DButton', frame)
			button:Dock(TOP)
			button:DockMargin(5, 5, 5, 5)
			button:SetZPos(30)
			button:SetText('gui.dsrpays.controls.delete_cppi')

			function button.DoClick()
				if not IsValid(spray) then return end
				net.Start('dsprays_delete_spray', true)
				net.WriteEntity(spray)
				net.WriteUInt8(1)
				net.SendToServer()
			end
		-- elseif ply:IsAdmin() then
		elseif DSprays.camiwatchdog:HasPermission('dsprays_admin') then
			local button = vgui.Create('DButton', frame)
			button:Dock(TOP)
			button:DockMargin(5, 5, 5, 5)
			button:SetZPos(30)
			button:SetText('gui.dsrpays.controls.admin_delete')

			function button.DoClick()
				if not IsValid(spray) then return end
				net.Start('dsprays_delete_spray', true)
				net.WriteEntity(spray)
				net.WriteUInt8(2)
				net.SendToServer()
			end
		end
	end

	if DSprays.camiwatchdog:HasPermission('dsprays_admin') then
		local button = vgui.Create('DButton', frame)
		button:Dock(TOP)
		button:DockMargin(5, 5, 5, 5)
		button:SetZPos(100)
		button:SetText('gui.dsrpays.controls.admin_get_url')

		function button.DoClick()
			SetClipboardText(spray:GetSprayURL())
		end
	end
end

local function ContextMenuClosed()
	if IsValid(frame) then
		frame:Remove()
	end
end

hook.Add('ContextMenuOpened', 'DSprays Spray Controls', ContextMenuOpened)
hook.Add('ContextMenuClosed', 'DSprays Spray Controls', ContextMenuClosed)

list.Set('DesktopWindows', 'DSprays', {
	title = 'DSprays',
	icon = 'icon64/tool.png',
	width = 960,
	height = 700,
	onewindow = true,
	init = function(icon, window)
		window:Remove()
		DSprays.OpenGUI()
	end
})

if IsValid(g_ContextMenu) then
	CreateContextMenu()
end
