
-- Copyright (C) 2020-2021 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

-- we don't do heavy transmissions
-- so we don't need DLib.Net for now
-- local net = DLib.Net

local dsprays_max_sprays = CreateConVar('dsprays_max_sprays', '3', {FCVAR_NOTIFY, FCVAR_ARCHIVE}, 'How many sprays a player can place')
local dsprays_delete_sprays = CreateConVar('dsprays_delete_sprays', '0', {FCVAR_NOTIFY, FCVAR_ARCHIVE}, 'Delete sprays on player disconnect')

net.pool('dsprays_spray')
net.pool('dsprays_delete_spray')
net.pool('dsprays_delete_spray_self')

local vector_up = Vector(0, 0, 1)

local function DSprays_MakeSprayEntity(ply, tr, index)
	if not istable(ply._dsprays_spray) then
		ply._dsprays_spray = {}
	end

	if IsValid(ply._dsprays_spray[index]) then
		ply._dsprays_spray[index]:Remove()
		ply._dsprays_spray[index] = nil
	end

	local sprays = {}

	for index, ent in pairs(ply._dsprays_spray) do
		if IsValid(ent) then
			ent._dsprays_stamp = ent._dsprays_stamp or SysTime()
			table.insert(sprays, ent)
		end
	end

	table.sort(sprays, function(a, b)
		return b._dsprays_stamp > a._dsprays_stamp
	end)

	if #sprays >= dsprays_max_sprays:GetInt() then
		for i = 1, #sprays - dsprays_max_sprays:GetInt() + 1 do
			sprays[i]:Remove()
		end
	end

	local ent = ents.Create('dspray')

	ent._dsprays_stamp = SysTime()
	ply._dsprays_spray[index] = ent
	ent._dsprays_index = index

	ent:SetPos(tr.HitPos)

	if tr.HitNormal:Dot(vector_up):abs() >= 0.9 then
		local get = tr.HitNormal:Angle()
		local dir = tr.HitPos - ply:GetPos()
		dir:Normalize()
		local dot = -get:Up():Dot(dir):acos():deg()

		if get:Right():Dot(dir) > 0 then dot = -dot end

		get:RotateAroundAxis(tr.HitNormal, dot)
		ent:SetAngles(get)
	elseif ply:GetActiveWeaponClass() == 'gmod_camera' then
		local ang = tr.HitNormal:Angle()
		ang.r = -ply:GetActiveWeapon():GetRoll()
		ent:SetAngles(ang)
	else
		ent:SetAngles(tr.HitNormal:Angle())
	end

	ent:SetSpraySteamID(ply:SteamID() or 'STEAM_0:0:0')
	ent:SetSpraySize(50)

	ent:SetRev(0)

	return ent
end

DSprays.MakeSprayEntity = DSprays_MakeSprayEntity

function DSprays.SprayDefault(ply, tr, size)
	tr = tr or ply:GetEyeTrace()

	local ent = DSprays_MakeSprayEntity(ply, tr, 'source')
	ent:SetSourceSprayPlayer(true)
	ent:SetSprayLabel('Source Engine spray')
	ent:Spawn()

	ply:EmitSound('SprayCan.Paint')

	if IsValid(tr.Entity) then
		local Lpos, Lang = WorldToLocal(ent:GetPos(), ent:GetAngles(), tr.Entity:GetPos(), tr.Entity:GetAngles())
		ent:SetPos(Lpos)
		ent:SetAngles(Lang)
		ent:SetMoveParent(tr.Entity)
		ent:SetSprayParent(tr.Entity)
	end
end

DSprays.SourceEngineSpray = DSprays.SprayDefault

local function DSprays_ReadSprayData(_net)
	if _net == nil then _net = net end

	local data = {}

	data.index = _net.ReadString()
	data.url = _net.ReadString():trim()
	data.rating = _net.ReadUInt8()
	data.spray_type = _net.ReadUInt8()

	if not DSprays.SprayTypeName[data.spray_type] then
		data.spray_type = DSprays.TYPE_STATIC_PICTURE
	end

	data.background = _net.ReadBool()

	if data.background then
		data.background = _net.ReadColor()
	else
		data.background = nil
	end

	data.label = _net.ReadString()
	data.size = _net.ReadUInt8():clamp(10, 100)

	return data
end

DSprays.ReadSprayData = DSprays_ReadSprayData

local function dsprays_spray(len, ply)
	if not ply:Alive() then return end
	if ply:GetObserverMode() ~= OBS_MODE_NONE then return end
	if ply._dsprays_cooldown and ply._dsprays_cooldown > SysTime() then return true end
	ply._dsprays_cooldown = SysTime() + 2

	local _data = DSprays_ReadSprayData(net)
	if not DSprays.ValidURL(_data.url) then return end

	local tr = util.TraceLine({
		start = ply:GetShootPos(),
		endpos = ply:GetShootPos() + ply:GetAimVector() * 128,
		filter = player.GetAll(),
	})

	if not tr.Hit then return end

	local ent = DSprays_MakeSprayEntity(ply, tr, _data.index)
	ent:LoadDataFromTable(_data)
	ent:Spawn()

	ply:EmitSound('SprayCan.Paint')

	if IsValid(tr.Entity) then
		local Lpos, Lang = WorldToLocal(ent:GetPos(), ent:GetAngles(), tr.Entity:GetPos(), tr.Entity:GetAngles())
		ent:SetPos(Lpos)
		ent:SetAngles(Lang)
		ent:SetMoveParent(tr.Entity)
		ent:SetSprayParent(tr.Entity)
	end
end

local function PlayerSpray(ply)
	if not ply:Alive() then return true end
	if ply:GetObserverMode() ~= OBS_MODE_NONE then return true end
	if ply._dsprays_cooldown and ply._dsprays_cooldown > SysTime() then return true end
	ply._dsprays_cooldown = SysTime() + 2

	local tr = util.TraceLine({
		start = ply:GetShootPos(),
		endpos = ply:GetShootPos() + ply:GetAimVector() * 128,
		filter = player.GetAll(),
	})

	if not tr.Hit then return true end
	-- if IsValid(tr.Entity) and tr.Entity.CPPIGetOwner and not (not IsValid(tr.Entity:CPPIGetOwner()) or tr.Entity:CPPIGetOwner() == ply) then return end

	DSprays.SprayDefault(ply, tr)

	return true
end

local function PlayerAuthed(ply, steamid)
	ply._dsprays_spray = {}

	for i, ent in ipairs(ents.FindByClass('dspray')) do
		if ent._dsprays_index and ent:GetSpraySteamID() == steamid then
			ply._dsprays_spray[ent._dsprays_index] = ent
		end
	end
end

local function PlayerDisconnected(ply)
	if not dsprays_delete_sprays:GetBool() then return end

	if istable(ply._dsprays_spray) then
		for index, ent in pairs(ply._dsprays_spray) do
			if IsValid(ent) then
				ent:Remove()
			end
		end
	end
end

local function dsprays_delete_spray(_, ply)
	if not IsValid(ply) then return end
	local ent = net.ReadEntity()
	if not IsValid(ent) or ent:GetClass() ~= 'dspray' then return end

	local mode = net.ReadUInt8()

	local gply = ent:GetSprayPlayer()
	local label = ent:GetSprayLabel()

	if mode == 0 then -- self
		if ent:GetSprayPlayer() ~= ply then return end
		ent:Remove()
		DSprays.LChatPlayer(ply, 'gui.dsrpays.controls.deleted', label)
		return
	elseif mode == 1 then -- cppi owned prop
		if not IsValid(ent:GetSprayParent()) or not ent:GetSprayParent().CPPIGetOwner or ent:GetSprayParent():CPPIGetOwner() ~= ply  then return end
		ent:Remove()

		if gply then
			DSprays.LChatPlayer(ply, 'gui.dsrpays.controls.deleted_cppi2', label, gply)
		else
			DSprays.LChatPlayer(ply, 'gui.dsrpays.controls.deleted_cppi', label)
		end

		return
	end

	CAMI.PlayerHasAccess(ply, 'dsprays_admin', function(has)
		if not has then return end
		if not IsValid(ply) then return end
		if not IsValid(ent) then return end

		ent:Remove()

		if gply then
			DSprays.LChatPlayer(ply, 'gui.dsrpays.controls.admin_deleted2', label, gply)
			DSprays.LMessage('gui.dsrpays.controls.admin_deleted_log2', ply, label, gply)
		else
			DSprays.LChatPlayer(ply, 'gui.dsrpays.controls.admin_deleted', label)
			DSprays.LMessage('gui.dsrpays.controls.admin_deleted_log', ply, label)
		end
	end)
end

net.Receive('dsprays_spray', dsprays_spray)
net.Receive('dsprays_delete_spray', dsprays_delete_spray)
hook.Add('PlayerSpray', 'DSprays Default Spray Handler', PlayerSpray, -1)
hook.Add('PlayerDisconnected', 'DSprays Disconnect Handler', PlayerDisconnected, -1)
hook.Add('PlayerAuthed', 'DSprays Connected Handler', PlayerAuthed, -1)
