
-- Copyright (C) 2020-2021 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

local DSprays = DSprays

local text_rt, text_material, bg_rt, bg_material

local render = render
local cam = cam
local RealTimeL = RealTimeL
local SysTime = SysTime
local FrameNumber = FrameNumber

DSprays.MESH_LIST = DSprays.MESH_LIST or {}
DSprays.DEFAULT_SPRAY_SIZE = 50

for i = #DSprays.MESH_LIST, 1, -1 do
	if not IsValid(DSprays.MESH_LIST[i]) then
		table.remove(DSprays.MESH_LIST, i)
	end
end

-- Meta of mesh of DSprays
-- allows to track mesh state and add various augments to it
-- change material
-- render it
-- and stuff
-- does not require to be "bound" to a single IMesh
local metaMesh = {}

AccessorFunc(metaMesh, 'material', 'Material')
AccessorFunc(metaMesh, 'flashlight_pass', 'FlashlightPass')
AccessorFunc(metaMesh, 'render_can', 'RenderCan')
AccessorFunc(metaMesh, 'ignore_light', 'IgnoreLight')
AccessorFunc(metaMesh, 'background_color', 'BackgroundColor')
AccessorFunc(metaMesh, 'data_feedback', 'RenderFeedback')

function metaMesh:SetMaterial(mat)
	if not self.is_valid then return end
	if self.material == mat then return end

	if self.material == DSprays.LoadingMaterial then
		DSprays.LoadingRefCount = DSprays.LoadingRefCount - 1
		self.ref_lock = false
	elseif mat == DSprays.LoadingMaterial then
		DSprays.LoadingRefCount = DSprays.LoadingRefCount + 1
		self.ref_lock = true
	end

	self.material = mat

	if self.render_mesh then
		self.render_mesh.Material = mat
	end
end

function metaMesh:ctor(origin, normal)
	local angle

	if type(normal) == 'Angle' then
		angle = normal
		normal = angle:Forward()
	end

	self.angle = angle
	self.origin = Vector(origin)
	self.normal = Vector(normal)

	self.flashlight_pass = true
	self.render_can = true

	self.pooled = false
	self.ref_lock = true
	self.ignore_light = false

	self.is_valid = true

	DSprays.LoadingRefCount = DSprays.LoadingRefCount + 1
	self.material = DSprays.LoadingMaterial

	self.text_layers = {}
	self.text_layers_c = ''

	self.base_matrix = Matrix()
	self.base_matrix:Translate(origin)
	self.base_matrix:Rotate(angle or normal:Forward())

	self.text_matrix = Matrix()
	self.text_matrix:Translate(origin)
	self.text_matrix:Translate(normal * 0.1)
	self.text_matrix:Rotate(angle or normal:Forward())

	self.background_matrix = Matrix()
	self.background_matrix:Translate(origin)
	self.background_matrix:Translate(normal * -0.1)
	self.background_matrix:Rotate(angle or normal:Forward())

	self.text_matrix_engine = Matrix()
	self.text_matrix_engine:Translate(normal * 0.1)

	self.background_matrix_engine = Matrix()
	self.background_matrix_engine:Translate(normal * -0.1)
end

function metaMesh:IsBasic()
	--return self.flashlight_pass and self.render_can and not self.ignore_light and #self.text_layers_c == 0
	return #self.text_layers_c == 0 and not self.background_color
end

function metaMesh:GetRenderMesh()
	if self.data_feedback then
		self.data_feedback.feedback = FrameNumber()
	end

	return self.render_mesh
end

function metaMesh:GetRenderMeshCurrentLayered()
	return self.render_mesh_layer
end

function metaMesh:PushText(index, text)
	if not self.is_valid then return end

	assert(isstring(index), 'not isstring(index)')
	assert(isstring(text), 'not isstring(text)')

	self.text_layers[index] = text
	self:RebuildText()
end

function metaMesh:PopText(index)
	if not self.is_valid then return end
	if not self.text_layers[index] then return end
	self.text_layers[index] = nil
	self:RebuildText()
end

function metaMesh:RebuildText()
	self.text_layers_c = ''

	for _, text in SortedPairs(self.text_layers) do
		self.text_layers_c = self.text_layers_c .. '\n' .. text
	end

	surface.SetFont('dsprays_text')
	self.text_layers_w, self.text_layers_h = surface.GetTextSize(self.text_layers_c)
end

function metaMesh:SetIMesh(mesh)
	if not self.is_valid then return end

	if IsValid(self.mesh) then
		self.mesh:Destroy()
	end

	self.mesh = mesh

	if not self.render_mesh then
		self.render_mesh = {Mesh = self.mesh, Material = self.material}
	end

	if not self.render_mesh_i then
		self.render_mesh_i = {Mesh = self.mesh, Material = self.material, Matrix = self.base_matrix}
	end

	if not self.render_mesh_background then
		self.render_mesh_background = {Mesh = self.mesh, Material = bg_material, Matrix = self.background_matrix_engine}
	end

	if not self.render_mesh_background_i then
		self.render_mesh_background_i = {Mesh = self.mesh, Material = bg_material, Matrix = self.background_matrix_engine}
	end

	if not self.render_mesh_text then
		self.render_mesh_text = {Mesh = self.mesh, Material = text_material, Matrix = self.text_matrix_engine}
	end

	if not self.render_mesh_text_i then
		self.render_mesh_text_i = {Mesh = self.mesh, Material = text_material, Matrix = self.text_matrix}
	end

	self.render_mesh.Mesh = mesh
	self.render_mesh_i.Mesh = mesh
	self.render_mesh_background.Mesh = mesh
	self.render_mesh_background_i.Mesh = mesh
	self.render_mesh_text.Mesh = mesh
	self.render_mesh_text_i.Mesh = mesh
end

function metaMesh:GetIMesh()
	return self.mesh
end

function metaMesh:Remove()
	if not self.is_valid then return end

	self:Unpool()

	self.is_valid = false

	if IsValid(self.mesh) then
		self.mesh:Destroy()
	end

	if IsValid(self.can) then
		self.can:Remove()
	end

	if self.material == DSprays.LoadingMaterial and self.ref_lock then
		DSprays.LoadingRefCount = DSprays.LoadingRefCount - 1
		self.ref_lock = false
	end
end

function metaMesh:Pool()
	if not self.is_valid then return end
	if self.pooled then return end

	self.pooled = true
	table.insert(DSprays.MESH_LIST, self)
end

function metaMesh:Unpool()
	if not self.is_valid then return end
	if not self.pooled then return end

	self.pooled = false

	for i, obj in ipairs(DSprays.MESH_LIST) do
		if obj == self then
			table.remove(DSprays.MESH_LIST, i)
			return
		end
	end
end

function metaMesh:IsValid()
	-- return self.mesh ~= nil and self.mesh:IsValid()
	return self.is_valid
end

function metaMesh:RenderCan()
	if not IsValid(self.can) then
		self.can = ClientsideModel('models/props_junk/popcan01a.mdl', RENDERGROUP_OTHER)
		self.can:SetNoDraw(true)
		self.can:SetPos(self.origin)

		table.insert(DSprays.CanDummies, self.can)
	end

	self.can.last_use = RealTimeL() + 30

	render.SetBlend(0)
	self.can:DrawModel()
	render.SetBlend(1)
end

local coroutine_create = coroutine.create
local coroutine_resume = coroutine.resume
local coroutine_status = coroutine.status
local coroutine_yield = coroutine.yield

function metaMesh:DoRenderEngineLayered(...)
	if not self.is_valid then return false end
	if not IsValid(self.mesh) then return false end

	if not self._render_layer_thread then
		self._render_layer_thread = coroutine_create(self.RenderEngineLayered)
	end

	local status = coroutine_resume(self._render_layer_thread, self, ...)

	if not status then
		self._render_layer_thread = nil
		return false
	end

	if coroutine_status(self._render_layer_thread) == 'dead' then
		self._render_layer_thread = nil
		return false
	end

	return true
end

function metaMesh:RenderEngineLayered(state)
	if self.background_color then
		render.PushRenderTarget(bg_rt)
		render.Clear(self.background_color.r, self.background_color.g, self.background_color.b, self.background_color.a)
		render.PopRenderTarget()

		self.render_mesh_layer = state and self.render_mesh_background_i or self.render_mesh_background
		coroutine_yield()
	end

	if self.material then
		self.render_mesh_layer = state and self.render_mesh_i or self.render_mesh

		if self.data_feedback then
			self.data_feedback.feedback = FrameNumber()
		end

		coroutine_yield()
	end

	local size = #self.text_layers_c
	if size ~= 0 then
		render.PushRenderTarget(text_rt)
		render.Clear(0, 0, 0, 0)

		render.SetWriteDepthToDestAlpha(false)

		cam.Start2D()

		draw.DrawText(self.text_layers_c, 'dsprays_text', 258, 256 - self.text_layers_h / 2, color_black, TEXT_ALIGN_CENTER)
		draw.DrawText(self.text_layers_c, 'dsprays_text', 256, 256 - self.text_layers_h / 2, color_white, TEXT_ALIGN_CENTER)

		cam.End2D()

		render.SetWriteDepthToDestAlpha(true)

		render.PopRenderTarget()

		self.render_mesh_layer = state and self.render_mesh_text_i or self.render_mesh_text
		coroutine_yield()
	end
end

function metaMesh:Render()
	if not self.is_valid then return end
	if not IsValid(self.mesh) then return end

	if self.render_can and not self.ignore_light then
		self:RenderCan()
	end

	if self.ignore_light then
		render.SuppressEngineLighting(true)
	end

	while self:DoRenderEngineLayered(true) do
		local obj = self:GetRenderMeshCurrentLayered()

		render.SetMaterial(obj.Material)

		cam.PushModelMatrix(obj.Matrix)
		obj.Mesh:Draw()

		if self.flashlight_pass and not self.ignore_light then
			render.PushFlashlightMode(true)
			obj.Mesh:Draw()
			render.PopFlashlightMode()
		end

		cam.PopModelMatrix()
	end
end

DSprays.MeshObject = DLib.CreateMoonClass('DSpraysMeshObj', metaMesh)

do
	local function reloadMats()
		text_rt = GetRenderTarget('dsprays_text', 512, 512)
		bg_rt = GetRenderTarget('dsprays_bg', 4, 4)

		text_material = CreateMaterial('dsprays_text', 'VertexLitGeneric', {
			['$basetexture'] = text_rt:GetName(),
			['$translucent'] = '1',
			['$alpha'] = '1',
			['$nocull'] = '1',
		})

		bg_material = CreateMaterial('dsprays_bg', 'VertexLitGeneric', {
			['$basetexture'] = bg_rt:GetName(),
			['$translucent'] = '1',
			['$alpha'] = '1',
			['$nocull'] = '1',
		})

		text_material:SetTexture('$basetexture', text_rt)
		bg_material:SetTexture('$basetexture', bg_rt)
	end

	reloadMats()
	hook.Add('InvalidateMaterialCache', 'DSprays Reload Text RT', reloadMats)
end
