
-- Copyright (C) 2020-2021 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

local empty = function() end
local check_empty = DSprays.CheckEmpty

local PANEL = {}

AccessorFunc(PANEL, '_background_color', 'BackgroundColor')
AccessorFunc(PANEL, '_background_enable', 'BackgroundColorEnable')

function PANEL:Init()
	self.valid = false
	self.draw_vgui = false

	self:SetCursor('hand')
end

function PANEL:SetURL(url)
	if self.url == url then return end

	if not DSprays.ValidURL(url) then
		self.url = false
		self.valid = false

		if IsValid(self.html_panel) then
			self.html_panel:Remove()
		end

		return
	end

	self.valid = true
	self.url = url

	self:ReloadTexture()
end

function PANEL:ReloadTexture()
	if not self.valid then return end

	if self.video then
		if not IsValid(self.html_panel) then
			self.html_panel = vgui.Create('DHTML', self)
			self.html_panel:Dock(FILL)

			self.html_panel.ConsoleMessage = empty
		end

		self.loading = false

		if self.layout then
			self.html_panel:SetHTML(DSprays.HTMLPageVideo(self.url, self:GetSize()))
			self.html_panel:Refresh()
		end
	else
		if IsValid(self.html_panel) then
			self.html_panel:Remove()
		end

		self.loading = true
		local size = DSprays.IsRatingCorrect(self.rating) and 128 or 16

		DSprays.StaticURLTexture(self.url, size, size):Then(function(data)
			if not IsValid(self) then return end
			self.loading = false
			self.data = data
		end)
	end
end

function PANEL:PerformLayout()
	self.layout = true

	if self.video then
		self:ReloadTexture()
	end
end

local icon_color_panel

function PANEL:Paint(w, h)
	if not self.valid then return end

	if self.background_color then
		surface.SetDrawColor(self.background_color)
		surface.DrawRect(0, 0, w, h)
	end

	if self.video then return end

	if self.loading then
		local min = math.min(w, h)
		DLib.HUDCommons.DrawLoading(0, 0, min, color_white, 50, math.clamp(min * 0.2, 5, 40))
		return
	end

	surface.SetDrawColor(255, 255, 255)
	surface.SetMaterial(self.data.material_gui)
	surface.DrawTexturedRect(0, 0, w, h)

	if not self.draw_vgui and not self.video and not IsValid(icon_color_panel) then
		hook.Add('PostRenderVGUI', self, self.PostRenderVGUI)
		self.draw_vgui = true
		self.vgui_material = nil
		self.vgui_loading = false
	end
end

function PANEL:OnBackgroundStatusChange(value)
	self._background_enable = value
	self:CheckBackgroundColor()

	if IsValid(self._target) then
		self._target:OnBackgroundStatusChange(value)
	end
end

function PANEL:CheckBackgroundColor()
	if self._background_enable then
		self.background_color = self._background_color
	else
		self.background_color = nil
	end
end

function PANEL:OnBackgroundColorChange(value)
	self._background_color = value
	self:CheckBackgroundColor()

	if IsValid(self._target) then
		self._target:OnBackgroundColorChange(value)
	end
end

function PANEL:OnMouseReleased(code)
	if not IsValid(self._target) then return end
	if code ~= MOUSE_LEFT then return end

	if IsValid(icon_color_panel) and icon_color_panel ~= self.color_panel then return end

	if IsValid(self.color_panel) then
		self.color_panel:MakePopup()
		icon_color_panel = self.color_panel
		return
	end

	local frame = vgui.Create('DLib_Window')
	frame:SetSize(500, 300)
	frame:Center()
	frame:MakePopup()
	frame:SetTitle('gui.dsrpays.main.background_color')

	self.color_panel = frame
	icon_color_panel = frame

	local enable_canvas = vgui.Create('DCheckBoxLabel', frame)
	enable_canvas:SetText('gui.dsrpays.main.background_color_enable')

	enable_canvas.Button:SetChecked(self._background_enable)

	function enable_canvas.Button.OnChange(_, value)
		self:OnBackgroundStatusChange(value)
	end

	enable_canvas:Dock(TOP)
	enable_canvas:DockMargin(0, 0, 0, 5)

	local mixer = vgui.Create('DLibColorMixer', frame)
	mixer:Dock(FILL)

	if self._background_color then
		mixer:SetColor(self._background_color)
	end

	function mixer.ValueChanged(_, value)
		self:OnBackgroundColorChange(value)
	end
end

do
	local w, h = 512, 512

	function PANEL:PostRenderVGUI()
		if not self:IsVisible() or self.video or IsValid(icon_color_panel) then
			self.draw_vgui = false
			hook.Remove('PostRenderVGUI', self)
			return
		end

		if not self:IsHovered() then return end

		local mx, my = input.GetCursorPos()
		local x, y = mx + 4, my + 4

		if x + w + 24 > ScrW() then
			x = mx - 4 - w
		end

		if y + h + 24 > ScrH() then
			y = my - 4 - h
		end

		if not self.vgui_material and not self.vgui_loading and self.url then
			self.vgui_loading = true

			DSprays.StaticURLTexture(self.url, w, h):Then(function(data)
				if not IsValid(self) then return end
				self.vgui_loading = false
				self.vgui_data = data
			end)
		end

		if self.background_color then
			surface.SetDrawColor(self.background_color)
		else
			surface.SetDrawColor(80, 80, 80)
		end

		surface.DrawRect(x, y, w, h)

		if self.vgui_loading then
			DLib.HUDCommons.DrawLoading(x + 2, y + 2, 512, color_black, 50, 30)
			DLib.HUDCommons.DrawLoading(x, y, 512, color_white, 50, 30)
			return
		end

		surface.SetDrawColor(255, 255, 255)
		surface.SetMaterial(self.vgui_data.material_gui)
		surface.DrawTexturedRect(x, y, w, h)
	end
end

function PANEL:SetRating(rating)
	if self.rating == rating then return end
	self.rating = rating
	hook.Add('DSprays_RatingChanged', self, self.ReloadTexture)
	self:ReloadTexture()
end

function PANEL:SetVideo(video)
	if self.video == video then return end
	self.video = video
	self:ReloadTexture()
end

vgui.Register('DSprays_Image', PANEL, 'EditablePanel')

local PANEL = {}

function PANEL:InitializePanels()
	if self.created_panels then return end

	self.icon = vgui.Create('DSprays_Image', self)
	self.label_canvas = vgui.Create('EditablePanel', self)
	self.move_canvas = vgui.Create('EditablePanel', self)
	self.label = vgui.Create('DTextEntry', self)
	self.url = vgui.Create('DTextEntry', self)
	self.bottom = vgui.Create('EditablePanel', self)
	self.main = vgui.Create('DCheckBox', self.bottom)
	self.main_label = vgui.Create('DLabel', self.bottom)
	self.hide = vgui.Create('DCheckBox', self.bottom)
	self.hide_label = vgui.Create('DLabel', self.bottom)
	self.rating = vgui.Create('DComboBox', self.bottom)
	self.label_label = vgui.Create('DLabel', self.label_canvas)
	self.label_url = vgui.Create('DLabel', self.label_canvas)
	self.label_bottom = vgui.Create('DLabel', self.label_canvas)
	self.spray_type = vgui.Create('DComboBox', self.bottom)
	self.spray_size = vgui.Create('DNumSlider', self.bottom)

	self.remove_button = vgui.Create('DButton', self.bottom)

	self.move_up = vgui.Create('DButton', self.move_canvas)
	self.move_down = vgui.Create('DButton', self.move_canvas)

	self.rating:SetSortItems(false)
	self.spray_type:SetSortItems(false)

	for i, label in ipairs(DSprays.RatingList) do
		self.rating:AddChoice(label, i)
	end

	self.rating:SetValue('gui.dsrpays.rating.rating')

	self.spray_type:AddChoice('gui.dsrpays.main.types.static', DSprays.TYPE_STATIC_PICTURE)
	self.spray_type:AddChoice('gui.dsrpays.main.types.dynamic', DSprays.TYPE_DYNAMIC_PICTURE)
	self.spray_type:AddChoice('gui.dsrpays.main.types.dynamic_video', DSprays.TYPE_DYNAMIC_VIDEO)

	self.spray_type:SetValue('gui.dsrpays.main.types.static')

	function self.label.OnKeyCode(_, key)
		if key == KEY_TAB or key == KEY_ENTER then
			timer.Simple(0, function()
				if not IsValid(self) then return end
				self.url:RequestFocus()
			end)
		end
	end

	function self.url.OnKeyCode(_, key)
		if key == KEY_TAB or key == KEY_ENTER then
			timer.Simple(0, function()
				if not IsValid(self) then return end

				if IsValid(self._target) then
					self._target:TabFocusNext(self)
				else
					self.label:RequestFocus()
				end
			end)
		end
	end

	self.icon:Dock(LEFT)
	self.label_canvas:Dock(LEFT)
	self.move_canvas:Dock(RIGHT)
	self.label:Dock(TOP)
	self.url:Dock(TOP)
	self.bottom:Dock(TOP)

	self.label_label:Dock(TOP)
	self.label_url:Dock(TOP)
	self.label_bottom:Dock(TOP)

	self.icon:SetZPos(-10)
	self.move_canvas:SetZPos(-1)
	self.label_canvas:SetZPos(0)
	self.label:SetZPos(1)
	self.url:SetZPos(2)
	self.bottom:SetZPos(3)

	self.main:Dock(LEFT)
	self.main_label:Dock(LEFT)
	self.hide:Dock(LEFT)
	self.hide_label:Dock(LEFT)
	self.rating:Dock(LEFT)
	self.spray_type:Dock(LEFT)
	self.spray_size:Dock(LEFT)
	self.remove_button:Dock(RIGHT)
	self.move_up:Dock(TOP)
	self.move_down:Dock(BOTTOM)

	self.main:SetZPos(0)
	self.main_label:SetZPos(1)
	self.hide:SetZPos(2)
	self.hide_label:SetZPos(3)
	self.spray_size:SetZPos(9)
	self.rating:SetZPos(10)
	self.remove_button:SetZPos(40)
	self.spray_type:SetZPos(5)

	self.icon:SetWide(96)
	self.move_canvas:SetWide(24)
	self.move_canvas:DockMargin(5, 0, 5, 0)

	self.move_up:DockMargin(0, 20, 0, 2)
	self.move_down:DockMargin(0, 2, 0, 20)

	self.spray_type:DockMargin(5, 0, 5, 0)

	self.bottom:SetTall(16)

	self.label_canvas:SetWide(100)

	self.label_label:SetText('gui.dsrpays.main.labels.label')
	self.label_url:SetText('gui.dsrpays.main.labels.url')
	self.label_bottom:SetText('gui.dsrpays.main.labels.bottom')

	self.main_label:SetText('gui.dsrpays.main.labels.main')
	self.hide_label:SetText('gui.dsrpays.main.labels.hide')
	self.remove_button:SetText('gui.dsrpays.main.remove_spray')

	self.spray_size:SetText('gui.dsrpays.main.spray_size')

	self.spray_size:SetMin(10)
	self.spray_size:SetMax(100)
	self.spray_size:SetDecimals(0)

	self.move_up:SetText('')
	self.move_up:SetIcon('icon16/arrow_up.png')
	self.move_down:SetText('')
	self.move_down:SetIcon('icon16/arrow_down.png')

	self.move_up:SetTall(24)
	self.move_down:SetTall(24)

	self.main_label:SizeToContents()
	self.hide_label:SizeToContents()

	self.label_label:SizeToContents()
	self.label_url:SizeToContents()
	self.label_bottom:SizeToContents()
	self.remove_button:SizeToContents()

	self.label_canvas:SetWide(math.max(self.label_label:GetWide(), self.label_url:GetWide(), self.label_bottom:GetWide()) + 12)
	self.label_label:SetWide(self.label_canvas:GetWide())
	self.label_url:SetWide(self.label_canvas:GetWide())
	self.label_bottom:SetWide(self.label_canvas:GetWide())

	self.remove_button:SetWide(self.remove_button:GetWide() + 20)

	self.label_label:DockMargin(5, 7, 0, 6)
	self.label_url:DockMargin(5, 7, 0, 6)
	self.label_bottom:DockMargin(5, 7, 0, 6)

	self.main_label:DockMargin(5, 0, 5, 0)
	self.hide_label:DockMargin(5, 0, 5, 0)

	self.label:DockMargin(0, 5, 0, 5)
	self.url:DockMargin(0, 5, 0, 5)
	self.bottom:DockMargin(0, 5, 0, 5)

	self.main:DockMargin(10, 0, 0, 0)

	self.label:SetUpdateOnType(true)
	self.url:SetUpdateOnType(true)

	function self.spray_size.Paint(_self, w, h)
		surface.SetDrawColor(140, 140, 140)
		surface.DrawRect(0, 0, w, h)
	end

	self.spray_size:DockPadding(5, 0, 0, 0)

	self.icon._target = self
	self.label._target = self
	self.url._target = self
	self.main._target = self
	self.hide._target = self
	self.rating._target = self
	self.spray_type._target = self
	self.spray_size._target = self
	self.remove_button._target = self
	self.move_up._target = self
	self.move_down._target = self

	self._cached_move_up = false
	self._cached_move_down = false

	self.created_panels = true
end

function PANEL:Init()
	self:SetSize(200, 96)
	self.created_panels = false
	self.reveal_ticks = 0
end

function PANEL:InnerUpdate()
	if not self._value then return end

	self.icon:SetURL(self._value.url)
	self.icon:SetRating(self._value.rating)
	self.icon:SetVideo(self._value.spray_type == DSprays.TYPE_DYNAMIC_VIDEO)
	self._target:RowUpdate(self, self._value, self._old_value)

	self.remove_button:SetEnabled(not check_empty(self._value) or self._target and self._target.determine_empty > 1)
end

function PANEL:IsDataEmpty()
	return not self._value or check_empty(self._value)
end

AccessorFunc(PANEL, '_cached_move_up', 'CachedMoveUp')
AccessorFunc(PANEL, '_cached_move_down', 'CachedMoveDown')

function PANEL:CanMoveUp()
	if not self._cached_move_up then return false end
	if check_empty(self._value) then return false end
	return true
end

function PANEL:CanMoveDown()
	if not self._cached_move_down then return false end
	if check_empty(self._value) then return false end
	return true
end

function PANEL:CalculateCanMove()
	if IsValid(self.move_up) then
		self.move_up:SetEnabled(self:CanMoveUp())
	end

	if IsValid(self.move_up) then
		self.move_down:SetEnabled(self:CanMoveDown())
	end
end

function PANEL:DoMoveUp()
	if IsValid(self._target) then
		if self._target:DoMoveUp(self) then
			--[[local x, y = input.GetCursorPos()
			y = y - self:GetTall()
			input.SetCursorPos(x, y)]]

			self._target:ScrollToChildCustom(self.move_up)

			return true
		end
	end

	return false
end

function PANEL:DoMoveDown()
	if IsValid(self._target) then
		if self._target:DoMoveDown(self) then
			self._target:ScrollToChildCustom(self.move_down)

			return true
		end
	end

	return false
end

function PANEL:DoDelete()
	self._old_value = self._value
	self._target:RowUpdate(self, nil, self._old_value)
	self:Remove()
end

function PANEL:OnBackgroundStatusChange(value)
	if not self._value then return end
	self._old_value = table.Copy(self._value)
	self._value.color_enable = value
	self:InnerUpdate()
end

function PANEL:OnBackgroundColorChange(value)
	if not self._value then return end
	self._old_value = table.Copy(self._value)
	self._value.color = value
	self:InnerUpdate()
end

function PANEL:PerformLayout(w, h)
	if not self.created_panels then return end

	self.rating:SetWide(math.clamp(w * 0.15, 80, 200))
	self.spray_type:SetWide(math.clamp(w * 0.15, 80, 200))
	self.spray_size:SetWide(math.clamp(w * 0.2, 80, 200))
end

function PANEL:FinishSetup(inputValue)
	if not self.created_panels then
		self._setup_value = inputValue or {
			url = '',
			label = '',
			main = false,
			hide = false,
			rating = 1,
			spray_size = 50,
			spray_type = DSprays.TYPE_STATIC_PICTURE,
		}

		self._value = inputValue or self._setup_value
		return self._value
	else
		return self:_FinishSetup(inputValue)
	end
end

function PANEL:Paint(w, h)
	if self.created_panels then return end

	surface.SetDrawColor(200, 200, 200, 100)
	surface.DrawRect(4, 4, 86, 86)
	surface.DrawRect(104, 4, w - 130, 20)
	surface.DrawRect(104, 30, w - 130, 20)

	self.reveal_ticks = self.reveal_ticks + 1
end

function PANEL:Think()
	if not self.created_panels then
		if self.body_visible then
			self.reveal_ticks = self.reveal_ticks + 1

			if self.reveal_ticks > 10 then
				self:InitializePanels()
				self:_FinishSetup(self._setup_value)
			end
		elseif self.reveal_ticks > 0 then
			self.reveal_ticks = self.reveal_ticks - 1
		end
	end
end

function PANEL:ThinkScrolled()
	if self.created_panels then return true end

	local _, y = self:GetPos()
	local _, yp = self:GetParent():GetPos()
	local _, h = self:GetParent():GetParent():GetSize()

	self.body_visible = y + yp < h and y + yp > -10
	return self.body_visible
end

function PANEL:_FinishSetup(inputValue)
	local merge_target = {}

	local rating = 1
	local spray_size = 50
	local spray_type = DSprays.TYPE_STATIC_PICTURE

	if istable(inputValue) then
		merge_target = inputValue

		if isstring(inputValue.label) then
			self.label:SetValue(inputValue.label)
		end

		if isstring(inputValue.url) then
			self.url:SetValue(inputValue.url)
		end

		if isbool(inputValue.main) then
			self.main:SetChecked(inputValue.main)
		end

		if isbool(inputValue.hide) then
			self.hide:SetChecked(inputValue.hide)
		end

		if isnumber(inputValue.rating) then
			rating = inputValue.rating
		end

		if isnumber(inputValue.spray_type) then
			spray_type = inputValue.spray_type
		end

		if isnumber(inputValue.spray_size) then
			spray_size = inputValue.spray_size
		end
	end

	if not DSprays.SprayTypeName[spray_type] then
		spray_type = DSprays.TYPE_STATIC_PICTURE
	end

	table.Merge(merge_target, {
		label = self.label:GetValue() or '',
		url = self.url:GetValue() or '',
		main = self.main:GetChecked() or false,
		hide = self.hide:GetChecked() or false,
		spray_type = spray_type,
		spray_size = spray_size,
		rating = rating,
	})

	self.remove_button:SetEnabled(not check_empty(merge_target))

	self.icon:SetRating(rating)
	self.icon:SetVideo(spray_type == DSprays.TYPE_DYNAMIC_VIDEO)
	self.icon:SetURL(merge_target.url)

	self.icon:SetBackgroundColorEnable(merge_target.color_enable)
	self.icon:SetBackgroundColor(merge_target.color)
	self.icon:CheckBackgroundColor()

	self.spray_size:SetValue(spray_size)

	self.rating:ChooseOptionID(rating)

	self.spray_type:ChooseOptionID(spray_type)

	self:CalculateCanMove()

	self._value = merge_target
	self._old_value = table.Copy(self._value)

	function self.label.OnValueChange(_self, _value)
		if not self._value then return end
		self._old_value = table.Copy(self._value)
		self._value.label = _value
		self:InnerUpdate()
	end

	function self.url.OnValueChange(_self, _value)
		if not self._value then return end
		self._old_value = table.Copy(self._value)
		self._value.url = _value
		self:InnerUpdate()
	end

	function self.main.OnChange(_self, _value)
		if not self._value then return end
		self._old_value = table.Copy(self._value)
		self._value.main = _value
		self:InnerUpdate()
	end

	function self.hide.OnChange(_self, _value)
		if not self._value then return end
		self._old_value = table.Copy(self._value)
		self._value.hide = _value
		self:InnerUpdate()
	end

	function self.rating.OnSelect(_self, index, value, data)
		if not self._value then return end
		self._old_value = table.Copy(self._value)
		self._value.rating = data
		self:InnerUpdate()
	end

	function self.spray_type.OnSelect(_self, index, value, data)
		if not self._value then return end
		self._old_value = table.Copy(self._value)
		self._value.spray_type = data
		self:InnerUpdate()
	end

	function self.spray_size.OnValueChanged(_self, value)
		if not self._value then return end
		self._old_value = table.Copy(self._value)
		self._value.spray_size = value:floor():clamp(10, 100)
		self:InnerUpdate()
	end

	function self.remove_button.DoClick(_self)
		self:DoDelete()
	end

	function self.move_up.DoClick(_self)
		self:DoMoveUp()
	end

	function self.move_down.DoClick(_self)
		self:DoMoveDown()
	end

	return self._value
end

function PANEL:GetValue()
	return self._value
end

vgui.Register('DSprays_Row', PANEL, 'EditablePanel')

local PANEL = {}

AccessorFunc(PANEL, '_data_index', 'DataIndex')
AccessorFunc(PANEL, '_meta_list', 'MetaList')

local delete_button_DoClick, rename_button_DoClick, hide_button_OnChange, random_button_OnChange, unique_indexing_OnChange, mass_add_DoClick

do
	local control_target

	local function delete_button_confirm()
		if not IsValid(control_target) then return end
		control_target:GalleryDelete()
	end

	function delete_button_DoClick(self)
		control_target = self._target
		Derma_Query('gui.dsrpays.misc.delete_gallery_desc', 'gui.dsrpays.misc.delete_gallery', 'gui.misc.confirm', delete_button_confirm, 'gui.misc.cancel')
	end

	local function rename_button_confirm(text)
		if not IsValid(control_target) then return end
		control_target:RenameGallery(text)
	end

	function rename_button_DoClick(self)
		control_target = self._target
		Derma_StringRequest('gui.dsrpays.misc.rename_gallery', 'gui.dsrpays.misc.rename_gallery_desc', '', rename_button_confirm, empty, 'gui.misc.ok', 'gui.misc.cancel')
	end

	function hide_button_OnChange(self, _value)
		self._target:GalleryHideUpdate(_value)
	end

	function random_button_OnChange(self, _value)
		self._target:GalleryRandomUpdate(_value)
	end

	function unique_indexing_OnChange(self, _value)
		self._target:GalleryUniqueIndexingUpdate(_value)
	end

	function mass_add_DoClick(self)
		local frame = vgui.Create('DLib_Window')
		frame:SetSize(500, 700)
		frame:Center()
		frame:SetTitle('gui.dsrpays.main.labels.mass_import')

		local label = vgui.Create('DLabel', frame)
		label:Dock(TOP)
		label:SetText('gui.dsrpays.main.labels.mass_import_tip')
		label:SizeToContents()
		label:DockMargin(5, 5, 5, 5)

		local button = vgui.Create('DButton', frame)
		button:Dock(BOTTOM)
		button:SetText('gui.misc.ok')
		button:DockMargin(5, 5, 5, 5)

		local entry = vgui.Create('DTextEntry', frame)
		entry:Dock(FILL)
		entry:SetMultiline(true)

		entry:DockMargin(5, 10, 5, 10)

		function button.DoClick()
			if not IsValid(self._target) then return end
			local self = self._target

			for i, url in ipairs(entry:GetValue():split('\n')) do
				local trim = url:trim()

				if trim ~= '' and DSprays.ValidURL(trim) then
					self:AddPlainRow(trim)
				end
			end

			self:MoveEmptyRow()

			frame:Close()
		end
	end
end

function PANEL:Init()
	self.determine_empty = 0
	self.row_index = 0
	self.rows = {}
	self.unlocked_load = 0
end

function PANEL:TabFocusNext(rowPrev)
	if not rowPrev.data_index then return end
	assert(self.rows[rowPrev.data_index] == rowPrev, 'self.rows[rowPrev.data_index] ~= rowPrev')
	if #self.rows == 1 then return end

	if not self.rows[rowPrev.data_index + 1] then
		self:ScrollToChildCustom(self.rows[1].label)
		self.rows[1].label:RequestFocus()
		return
	end

	self:ScrollToChildCustom(self.rows[rowPrev.data_index + 1].label)
	self.rows[rowPrev.data_index + 1].label:RequestFocus()
end

function PANEL:CreateControlPanel()
	if IsValid(self.controls) then return self.controls end

	self.controls = vgui.Create('EditablePanel', self)

	self.controls:SetZPos(-5)
	self.controls:Dock(TOP)
	self.controls:DockMargin(5, 0, 5, 5)

	self.controls:SetTall(20)

	return self.controls
end

function PANEL:AllowDeleting()
	self.delete_button = vgui.Create('DButton', self:CreateControlPanel())
	self.delete_button:Dock(RIGHT)
	self.delete_button:DockMargin(5, 0, 5, 0)
	self.delete_button:SetText('gui.dsrpays.misc.delete_gallery')
	self.delete_button._target = self
	self.delete_button.DoClick = delete_button_DoClick

	self.delete_button:SizeToContents()
	self.delete_button:SetWide(self.delete_button:GetWide() + 20)

	self.delete_button:SetZPos(0)
end

function PANEL:AllowRenaming()
	self.rename_button = vgui.Create('DButton', self:CreateControlPanel())
	self.rename_button:Dock(LEFT)
	self.rename_button:DockMargin(5, 0, 5, 0)
	self.rename_button:SetText('gui.dsrpays.misc.rename_gallery')
	self.rename_button._target = self
	self.rename_button.DoClick = rename_button_DoClick

	self.rename_button:SizeToContents()
	self.rename_button:SetWide(self.rename_button:GetWide() + 20)

	self.rename_button:SetZPos(1)
end

function PANEL:AllowHiding()
	self.hide_button = vgui.Create('DCheckBox', self:CreateControlPanel())
	self.hide_button:Dock(LEFT)
	self.hide_button:DockMargin(5, 0, 5, 0)
	self.hide_button._target = self

	if self._target_meta then
		self.hide_button:SetChecked(self._target_meta.hide or false)
	end

	self.hide_button.OnChange = hide_button_OnChange

	self.hide_text = vgui.Create('DLabel', self:CreateControlPanel())
	self.hide_text:SetText('gui.dsrpays.main.labels.hide')
	self.hide_text:Dock(LEFT)
	self.hide_text:DockMargin(0, 0, 5, 0)

	self.hide_text:SizeToContents()

	self.hide_button:SetZPos(2)
	self.hide_text:SetZPos(3)
end

function PANEL:AllowRandomizing()
	self.random_button = vgui.Create('DCheckBox', self:CreateControlPanel())
	self.random_button:Dock(LEFT)
	self.random_button:DockMargin(5, 0, 5, 0)
	self.random_button._target = self

	if self._target_meta then
		self.random_button:SetChecked(self._target_meta.random or false)
	end

	self.random_button.OnChange = random_button_OnChange

	self.random_text = vgui.Create('DLabel', self:CreateControlPanel())
	self.random_text:SetText('gui.dsrpays.main.labels.random')
	self.random_text:Dock(LEFT)
	self.random_text:DockMargin(0, 0, 5, 0)

	self.random_text:SizeToContents()

	self.random_button:SetZPos(4)
	self.random_text:SetZPos(5)
end

function PANEL:AllowUniqueIndexing()
	self.unique_indexing = vgui.Create('DCheckBox', self:CreateControlPanel())
	self.unique_indexing:Dock(LEFT)
	self.unique_indexing:DockMargin(5, 0, 5, 0)
	self.unique_indexing._target = self

	if self._target_meta then
		self.unique_indexing:SetChecked(self._target_meta.unique_indexing or false)
	end

	self.unique_indexing.OnChange = unique_indexing_OnChange

	self.unique_indexing_text = vgui.Create('DLabel', self:CreateControlPanel())
	self.unique_indexing_text:SetText('gui.dsrpays.main.labels.unique_indexing')
	self.unique_indexing_text:Dock(LEFT)
	self.unique_indexing_text:DockMargin(0, 0, 5, 0)

	self.unique_indexing_text:SizeToContents()

	self.unique_indexing:SetZPos(6)
	self.unique_indexing_text:SetZPos(7)

	self.unique_indexing:SetTooltip('gui.dsrpays.main.labels.unique_indexing_tip')
	self.unique_indexing_text:SetTooltip('gui.dsrpays.main.labels.unique_indexing_tip')
end

function PANEL:AllowMassAddition()
	self.mass_add = vgui.Create('DButton', self:CreateControlPanel())
	self.mass_add:Dock(RIGHT)
	self.mass_add:DockMargin(5, 0, 5, 0)
	self.mass_add._target = self

	self.mass_add.DoClick = mass_add_DoClick

	self.mass_add:SetZPos(6)

	self.mass_add:SetText('gui.dsrpays.main.labels.mass_import')
	self.mass_add:SizeToContents()
	self.mass_add:SetWide(self.mass_add:GetWide() + 20)
end

function PANEL:AllowEverything()
	self:AllowDeleting()
	self:AllowRenaming()
	self:AllowHiding()
	self:AllowRandomizing()
	self:AllowUniqueIndexing()
	self:AllowMassAddition()
end

local function DoAlignCursor(self)
	local w, h = self:GetSize()
	local X, Y = self:LocalToScreen(w / 2, h / 2)
	input.SetCursorPos(X, Y)

	hook.Remove('Think', self)
end

function PANEL:ScrollToChildCustom(child)
	-- make sure our size is all good
	self:InvalidateLayout(true)

	local x, y = self:GetCanvas():GetChildPosition(child)
	local w, h = child:GetSize()

	y = y + h * 0.5
	y = y - self:GetTall() * 0.5

	self.VBar:SetScroll(y)

	hook.Add('Think', child, DoAlignCursor)
end

function PANEL:OnGalleryDelete()

end

function PANEL:GalleryDelete()
	if IsValid(self._button) then
		self._button:Remove()
	end

	if IsValid(self._meta_list) then
		self._meta_list:GalleryDelete(self)
	end

	self:OnGalleryDelete()
	self:Remove()
end

function PANEL:GalleryHideUpdate(_value)
	if IsValid(self._meta_list) then
		self._meta_list:GalleryHideUpdate(self, _value)
	end
end

function PANEL:GalleryRandomUpdate(_value)
	if IsValid(self._meta_list) then
		self._meta_list:GalleryRandomUpdate(self, _value)
	end
end

function PANEL:GalleryUniqueIndexingUpdate(_value)
	if IsValid(self._meta_list) then
		self._meta_list:GalleryUniqueIndexingUpdate(self, _value)
	end
end

function PANEL:RenameGallery(name)
	if IsValid(self._meta_list) and self._data_index then
		self._meta_list:RenameGallery(self._data_index, name)
	end
end

function PANEL:GetRows()
	return self.rows
end

function PANEL:GetNonEmptyValues()
	local values = {}

	for i, row in ipairs(self.rows) do
		local data = row:GetValue()

		if not check_empty(data) then
			table.insert(values, data)
		end
	end

	return values
end

function PANEL:OnRowUpdate(row, new_state, old_state)

end

function PANEL:RowUpdate(row, new_state, old_state)
	if new_state then
		local e1, e2 = check_empty(old_state), check_empty(new_state)

		if e1 and not e2 then
			self.determine_empty = self.determine_empty - 1
			self:AddEmptyRow()

			self:RefreshMovability()
		elseif not e1 and e2 then
			self.determine_empty = self.determine_empty + 1

			self:RefreshMovability()
		end

		if new_state.main ~= old_state.main and new_state.main then
			-- force every other "main" flag to off

			for i, row2 in ipairs(self.rows) do
				if row ~= row2 then
					local data = row2:GetValue()

					if data.main then
						row2.main:SetChecked(false)
						row2.main:OnChange(false) -- trigger event
					end
				end
			end
		end

		if new_state.hide ~= old_state.hide and new_state.hide and new_state.main then
			row.main:SetChecked(false)
			row.main:OnChange(false)
		end
	else
		for i, _row in ipairs(self.rows) do
			if row == _row then
				table.remove(self.rows, i)
				table.remove(self._target_data, i)

				for i2 = i, #self.rows do
					self.rows[i2].data_index = i2
				end

				break
			end
		end

		if check_empty(old_state) then
			self.determine_empty = self.determine_empty - 1
		end

		self:RefreshMovability()
	end

	self:OnRowUpdate(row, new_state, old_state)

	if IsValid(self._meta_list) then
		self._meta_list:OnRowUpdate(self, row, new_state, old_state)

		if self._data_index then
			self._meta_list:OnDataUpdate(self._data_index)
		end
	end
end

function PANEL:Populate()
	if self._populated then return end
	self._populated = true

	local canvas = self:GetCanvas()
	local hasEmpty = false

	for i, data in ipairs(self._target_data) do
		if not check_empty(data) then
			local row = vgui.Create('DSprays_Row', canvas)
			row:FinishSetup(data)
			row:Dock(TOP)
			row:SetZPos(i)
			row.data_index = i
			self.rows[i] = row
			row._target = self
		elseif not hasEmpty then
			hasEmpty = true
			self.determine_empty = self.determine_empty + 1

			local row = vgui.Create('DSprays_Row', canvas)
			row:FinishSetup(data)
			row:Dock(TOP)
			row:SetZPos(i)
			self.rows[i] = row
			row.data_index = i
			row._target = self
		end
	end

	self.row_index = #self._target_data + 1

	if not hasEmpty then
		self:AddEmptyRow()
	end

	if IsValid(self.hide_button) then
		self.hide_button:SetChecked(self._target_meta.hide or false)
	end

	self:RefreshMovability()
end

function PANEL:SetupData(data, meta)
	self._target_data = data or {}
	self._target_meta = meta or {
		name = 'Unnamed',
		hide = false,
	}

	return self._target_data, self._target_meta
end

function PANEL:DoMoveUp(row)
	for i = row.data_index - 1, 1, -1 do
		local row2 = self.rows[i]

		if not row2:IsDataEmpty() then
			self.rows[i] = row
			self.rows[row.data_index] = row2

			row2.data_index = row.data_index
			row.data_index = i

			self._target_data[row.data_index] = row:GetValue()
			self._target_data[row2.data_index] = row2:GetValue()

			row:SetZPos(row.data_index)
			row2:SetZPos(row2.data_index)

			local a, b, c, d = row:GetCachedMoveUp(), row:GetCachedMoveDown(), row2:GetCachedMoveUp(), row2:GetCachedMoveDown()

			row:SetCachedMoveUp(c)
			row:SetCachedMoveDown(d)
			row2:SetCachedMoveUp(a)
			row2:SetCachedMoveDown(b)

			row:CalculateCanMove()
			row2:CalculateCanMove()

			if IsValid(self._meta_list) then
				self._meta_list:OnTopDataUpdate(self._data_index)
			end

			return true, row2
		end
	end

	return false
end

function PANEL:DoMoveDown(row)
	for i = row.data_index + 1, #self.rows do
		local row2 = self.rows[i]

		if not row2:IsDataEmpty() then
			self.rows[i] = row
			self.rows[row.data_index] = row2

			row2.data_index = row.data_index
			row.data_index = i

			self._target_data[row.data_index] = row:GetValue()
			self._target_data[row2.data_index] = row2:GetValue()

			row:SetZPos(row.data_index)
			row2:SetZPos(row2.data_index)

			local a, b, c, d = row:GetCachedMoveUp(), row:GetCachedMoveDown(), row2:GetCachedMoveUp(), row2:GetCachedMoveDown()

			row:SetCachedMoveUp(c)
			row:SetCachedMoveDown(d)
			row2:SetCachedMoveUp(a)
			row2:SetCachedMoveDown(b)

			row:CalculateCanMove()
			row2:CalculateCanMove()

			if IsValid(self._meta_list) then
				self._meta_list:OnTopDataUpdate(self._data_index)
			end

			return true, row2
		end
	end

	return false
end

function PANEL:RefreshMovability()
	local upperRow, upperIndex

	for i = 1, #self.rows do
		if not check_empty(self.rows[i]:GetValue()) then
			upperRow = self.rows[i]
			upperIndex = i
			break
		end
	end

	if not upperRow or upperIndex == #self.rows then
		for i, row in ipairs(self.rows) do
			row:SetCachedMoveUp(false)
			row:SetCachedMoveDown(false)
			row:CalculateCanMove()
		end

		return
	end

	local bottomRow, bottomIndex

	for i = #self.rows, 1, -1 do
		if not check_empty(self.rows[i]:GetValue()) then
			bottomRow = self.rows[i]
			bottomIndex = i
			break
		end
	end

	if not upperRow or upperIndex == bottomIndex then
		for i, row in ipairs(self.rows) do
			row:SetCachedMoveUp(false)
			row:SetCachedMoveDown(false)
			row:CalculateCanMove()
		end

		return
	end

	upperRow:SetCachedMoveUp(false)
	upperRow:SetCachedMoveDown(true)
	upperRow:CalculateCanMove()

	for i = upperIndex + 1, bottomIndex - 1 do
		local row = self.rows[i]
		row:SetCachedMoveUp(true)
		row:SetCachedMoveDown(true)
		row:CalculateCanMove()
	end

	bottomRow:SetCachedMoveUp(true)
	bottomRow:SetCachedMoveDown(false)
	bottomRow:CalculateCanMove()
end

function PANEL:AddEmptyRow()
	if self.determine_empty > 0 then return end

	local row = vgui.Create('DSprays_Row', self:GetCanvas())
	row:InitializePanels()
	local value = row:FinishSetup()
	row:Dock(TOP)
	row:SetZPos(self.row_index)
	row._target = self
	row.data_index = table.insert(self._target_data, value)
	self.rows[row.data_index] = row

	self.row_index = self.row_index + 1
	self.determine_empty = 1
end

function PANEL:AddPlainRow(url, name)
	local row = vgui.Create('DSprays_Row', self:GetCanvas())

	if isstring(url) then
		row:InitializePanels()
		row.url:SetValue(url)
	end

	if isstring(name) then
		row:InitializePanels()
		row.name:SetValue(name)
	end

	local value = row:FinishSetup()
	row:Dock(TOP)
	row:SetZPos(self.row_index)
	row._target = self
	row.data_index = table.insert(self._target_data, value)
	self.rows[row.data_index] = row

	self.row_index = self.row_index + 1
end

function PANEL:MoveEmptyRow()
	for i, row in ipairs(self.rows) do
		if check_empty(row._value) then
			local oldIndex = row.data_index
			self.row_index = self.row_index + 1

			row:SetZPos(self.row_index)
			row.data_index = #self._target_data
			table.remove(self._target_data, oldIndex)
			table.remove(self.rows, oldIndex)
			self._target_data[row.data_index] = value
			self.rows[row.data_index] = row

			if self._data_index and IsValid(self._meta_list) then
				self._meta_list:OnDataUpdate(self._data_index)
			end

			return
		end
	end
end

function PANEL:Think()
	self.unlocked_load = self.unlocked_load + 1

	if not self.scroll_to_active_once and self.unlocked_load > 4 then
		self.scroll_to_active_once = true

		for i, row in ipairs(self.rows) do
			if row:GetValue().main then
				self:ScrollToChildCustom(row)
				break
			end
		end
	end

	if not self.vscroll_once and self.unlocked_load > 10 then
		self.vscroll_once = true

		for i, row in ipairs(self.rows) do
			row:ThinkScrolled()
		end
	end
end

function PANEL:OnVScroll(iOffset)
	DScrollPanel.OnVScroll(self, iOffset)

	if self.unlocked_load > 10 then
		self.vscroll_once = true

		for i, row in ipairs(self.rows) do
			row:ThinkScrolled()
		end
	end
end

vgui.Register('DSprays_RowList', PANEL, 'DScrollPanel')

local PANEL = {}

local create_new_confirm_self

local function create_new_confirm(text)
	if not IsValid(create_new_confirm_self) then return end
	create_new_confirm_self:GalleryCreate(text)
end

local function create_new_DoClick(self)
	if not self._target._meta_list then return end
	create_new_confirm_self = self._target
	Derma_StringRequest('gui.dsrpays.misc.create_gallery', 'gui.dsrpays.misc.create_gallery_desc', '', create_new_confirm, empty, 'gui.misc.ok', 'gui.misc.cancel')
end

function PANEL:Init()
	self.scroll = vgui.Create('DScrollPanel', self)
	self.createNew = vgui.Create('DButton', self)

	self.createNew:SetText('gui.dsrpays.misc.create_gallery')

	self.scroll:SetZPos(0)
	self.createNew:SetZPos(1)

	self.scroll:Dock(FILL)
	self.createNew:Dock(BOTTOM)

	self.buttons = {}
	self.lists = {}

	self.on_update = empty

	self.next_index = 0

	self.createNew._target = self
	self.createNew.DoClick = create_new_DoClick
end

function PANEL:SetCanvas(target)
	self._target = target
end

local function choose_data_DoClick(self)
	if not IsValid(self._target) then return end
	if self._meta_list._last_canvas == self._target then return end

	if IsValid(self._meta_list._last_canvas) then
		self._meta_list._last_canvas:SetVisible(false)
	end

	self._target:SetVisible(true)
	self._target:Populate()
	self._meta_list._last_canvas = self._target
end

function PANEL:MakePanels(meta, data, index)
	local button = vgui.Create('DButton', self.scroll:GetCanvas())
	button:Dock(TOP)

	if index then
		self.buttons[index] = button
	else
		table.insert(self.buttons, button)
	end

	local canvas = vgui.Create('DSprays_RowList', self)
	canvas:SetVisible(false)

	if IsValid(self._target) then
		canvas:SetParent(self._target)
	end

	canvas:Dock(FILL)

	if index then
		self.lists[index] = canvas
	else
		table.insert(self.lists, canvas)
	end

	canvas:SetupData(data, meta)

	canvas:SetDataIndex(index)

	button._meta_list = self
	button._target = canvas
	canvas._button = button

	button.DoClick = choose_data_DoClick
	button:SetText(meta.name or index or 'Unnamed')

	canvas._meta_list = self

	return button, canvas
end

function PANEL:OpenCanvas(index)
	if IsValid(self.buttons[index]) then
		self.buttons[index]:DoClick()
	end
end

function PANEL:GalleryHideUpdate(list, state)
	local index = list:GetDataIndex()
	if not index then return end

	local meta = self._meta_list.list[index]
	if not meta then return end

	local old_meta = table.Copy(meta)
	meta.hide = state

	self:OnMetaUpdate(index, meta, old_meta)
end

function PANEL:GalleryRandomUpdate(list, state)
	local index = list:GetDataIndex()
	if not index then return end

	local meta = self._meta_list.list[index]
	if not meta then return end

	local old_meta = table.Copy(meta)
	meta.random = state

	self:OnMetaUpdate(index, meta, old_meta)
end

function PANEL:GalleryUniqueIndexingUpdate(list, state)
	local index = list:GetDataIndex()
	if not index then return end

	local meta = self._meta_list.list[index]
	if not meta then return end

	local old_meta = table.Copy(meta)
	meta.unique_indexing = state

	self:OnMetaUpdate(index, meta, old_meta)
end

function PANEL:GalleryDelete(list)
	local index = list:GetDataIndex()
	if not index then return end

	if not self._meta_list or not self._meta_list.list[index] then return end

	local meta = self._meta_list.list[index]
	local data = self._data_list[index]

	self._meta_list.list[index] = nil
	self._data_list[index] = nil

	self:OnDataDelete(index)

	self:OnRootDataUpdate()
	self:OnRootMetaUpdate()
end

function PANEL:GalleryCreate(name)
	local index = 'gallery_' .. self._meta_list.next_id

	self._meta_list.list[index] = {
		name = name,
		hide = false,
	}

	self._data_list[index] = {}

	local button, canvas = self:MakePanels(self._meta_list.list[index], self._data_list[index], index)

	canvas:AllowEverything()

	button:DoClick()

	self:OnMetaUpdate(index, self._meta_list.list[index])
	self:OnDataUpdate(index)

	self._meta_list.next_id = self._meta_list.next_id + 1
	self:OnRootMetaUpdate()
end

function PANEL:OnRowUpdate(list, row, new_state, old_state)

end

function PANEL:OnDataUpdate(index)

end

function PANEL:OnDataDelete(index)

end

function PANEL:OnRootDataUpdate()

end

function PANEL:OnTopDataUpdate(index)

end

function PANEL:OnMetaUpdate(index, new_state, old_state)

end

function PANEL:OnRootMetaUpdate()

end

function PANEL:RenameGallery(index, name)
	if not self._meta_list.list[index] then return end
	self._meta_list.list[index].name = name

	if IsValid(self.buttons[index]) then
		self.buttons[index]:SetText(name)
	end
end

function PANEL:SetupData(metalist, datalist)
	self._meta_list = metalist
	self._data_list = datalist

	if isfunction(on_update) then
		self.on_update = on_update
	end

	local button

	if metalist.list.main and datalist.main then
		local canvas
		button, canvas = self:MakePanels(metalist.list.main, datalist.main, 'main')
		canvas:AllowRandomizing()
		canvas:AllowUniqueIndexing()
		canvas:AllowMassAddition()
	end

	for name, data in SortedPairs(metalist.list) do
		if name ~= 'main' and datalist[name] then
			local button2, canvas = self:MakePanels(data, datalist[name], name)

			if not button then button = button2 end

			canvas:AllowEverything()
		end
	end

	if IsValid(button) then
		button:DoClick()
	end
end

vgui.Register('DSprays_MetaList', PANEL, 'EditablePanel')

