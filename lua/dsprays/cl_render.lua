
-- Copyright (C) 2020-2021 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

local DSprays = DSprays

if DSprays.CanDummies then
	for i, ent in ipairs(DSprays.CanDummies) do
		SafeRemoveEntity(ent)
	end
end

DSprays.CanDummies = {}
DSprays.LoadingRefCount = math.max(DSprays.LoadingRefCount or 0, 0)

local loading_rt

local function Think()
	if DSprays.LoadingRefCount <= 0 and not DSprays.WheelGUIActive then return end

	render.PushRenderTarget(loading_rt)
	render.Clear(0, 0, 0, 0)
	render.SetWriteDepthToDestAlpha(false)
	cam.Start2D()

	DLib.HUDCommons.DrawLoading(6, 6, 500, color_white, 50, 40)

	cam.End2D()
	render.SetWriteDepthToDestAlpha(true)
	render.PopRenderTarget()
end

local render = render
local cam = cam
local RealTimeL = RealTimeL

local function inner()
	for i, mesh in ipairs(DSprays.MESH_LIST) do
		mesh:Render()
	end
end

local function PostDrawTranslucentRenderables(a, b)
	if a or b then return end

	render.SetLightingMode(0)
	render.PushFilterMag(TEXFILTER.ANISOTROPIC)

	ProtectedCall(inner)

	render.PopFilterMag()
end

surface.CreateFont('dsprays_text', {
	font = 'PT Sans Caption',
	size = 36,
	weight = 700,
	extended = true
})

local function reloadMats()
	loading_rt = GetRenderTarget('dsprays_loading', 512, 512)

	DSprays.SourceEngineSprayMissingRT = GetRenderTarget('dsprays_source_engine', 512, 512)

	DSprays.LoadingMaterial = CreateMaterial('dsprays_loading', 'VertexLitGeneric', {
		['$basetexture'] = loading_rt:GetName(),
		['$translucent'] = '1',
		['$alpha'] = '1',
		['$nocull'] = '1',
	})

	DSprays.SourceEngineSprayMissing = CreateMaterial('dsprays_source_engine', 'VertexLitGeneric', {
		['$basetexture'] = DSprays.SourceEngineSprayMissingRT:GetName(),
		['$translucent'] = '1',
		['$alpha'] = '1',
		['$nocull'] = 1,
	})

	DSprays.LoadingMaterialGUI = CreateMaterial('dsprays_loading_gui', 'UnlitGeneric', {
		['$basetexture'] = loading_rt:GetName(),
		['$translucent'] = '1',
		['$alpha'] = '1',
		['$nocull'] = '1',
	})

	DSprays.SourceEngineSprayMissingGUI = CreateMaterial('dsprays_source_engine_gui', 'UnlitGeneric', {
		['$basetexture'] = DSprays.SourceEngineSprayMissingRT:GetName(),
		['$translucent'] = '1',
		['$alpha'] = '1',
		['$nocull'] = '1',
	})

	DSprays.LoadingMaterial:SetTexture('$basetexture', loading_rt)
	DSprays.SourceEngineSprayMissing:SetTexture('$basetexture', DSprays.SourceEngineSprayMissingRT)
	DSprays.LoadingMaterialGUI:SetTexture('$basetexture', loading_rt)
	DSprays.SourceEngineSprayMissingGUI:SetTexture('$basetexture', DSprays.SourceEngineSprayMissingRT)

	render.PushRenderTarget(DSprays.SourceEngineSprayMissingRT)
	render.Clear(40, 40, 40, 150, true, true)

	cam.Start2D()

	surface.SetDrawColor(0, 0, 0)
	surface.DrawLine(1, 1, 1, 511)
	surface.DrawLine(1, 1, 511, 1)
	surface.DrawLine(1, 1, 511, 511)
	surface.DrawLine(511, 1, 1, 511)

	surface.DrawLine(511, 511, 1, 511)
	surface.DrawLine(511, 511, 511, 1)

	draw.DrawText(DLib.I18n.Localize('gui.dsrpays.misc.source_spray_missing'), 'dsprays_text', 258, 258, color_black, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
	draw.DrawText(DLib.I18n.Localize('gui.dsrpays.misc.source_spray_missing'), 'dsprays_text', 256, 256, color_white, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)

	cam.End2D()

	render.PopRenderTarget()
end

reloadMats()

hook.Add('InvalidateMaterialCache', 'DSprays Reload Rendertargets', reloadMats)
hook.Add('DLib.LanguageChanged', 'DSprays Reload Rendertargets', reloadMats)
hook.Add('PostDrawTranslucentRenderables', 'DSprays Draw', PostDrawTranslucentRenderables)
hook.Add('Think', 'DSprays Loading Render Target Update', Think)
