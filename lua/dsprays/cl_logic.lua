
-- Copyright (C) 2020-2021 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

local function dsprays_spray_advanced(index)
	if not DSprays.SaveData.data[index] then return false end
	if not DSprays.SaveData.meta.list[index] then return false end

	local candidate

	if DSprays.SaveData.meta.list[index].random then
		local count = 0

		for i, data in ipairs(DSprays.SaveData.data[index]) do
			if not data.hide and DSprays.ValidURL(data.url) then
				count = count + 1
			end
		end

		if count == 0 then return false end

		local rand = math.random(1, count)
		count = 0

		for i, data in ipairs(DSprays.SaveData.data[index]) do
			if not data.hide and DSprays.ValidURL(data.url) then
				count = count + 1

				if count == rand then
					candidate = data
					break
				end
			end
		end
	else
		candidate = select(2, DSprays.MainFromData(DSprays.SaveData.data[index]))
	end

	return DSprays.RequestSprayWithData(index, candidate)
end

function DSprays.WriteSprayDataToNetwork(sprayData, index, _net)
	if _net == nil then _net = net end

	_net.WriteString(index or DLib.Util.RandomUUID())
	_net.WriteString(sprayData.url)
	_net.WriteUInt8(sprayData.rating or 1)
	_net.WriteUInt8(sprayData.spray_type or DSprays.TYPE_STATIC_PICTURE)
	_net.WriteBool(sprayData.color_enable and IsColor(sprayData.color))

	if sprayData.color_enable and IsColor(sprayData.color) then
		_net.WriteColor(sprayData.color)
	end

	_net.WriteString(sprayData.label or '')
	_net.WriteUInt8(sprayData.spray_size or 50)
end

function DSprays.RequestSprayWithData(index, sprayData)
	if not sprayData then return false end

	local retVal = hook.Run('DSprays_RequestSpray', index, sprayData)
	if retVal ~= nil then return retVal end

	net.Start('dsprays_spray', true)
	DSprays.WriteSprayDataToNetwork(sprayData, DSprays.SaveData.meta.list[index].unique_indexing and (index .. '_' .. sprayData.url) or index, net)
	net.SendToServer()

	return true
end

local function dsprays_spray(notifyPlayer)
	if notifyPlayer == nil then notifyPlayer = true end

	if not dsprays_spray_advanced('main') then
		RunConsoleCommand('impulse', '201')

		if notifyPlayer and not DSprays.NotifiedLocalPlayer then
			DSprays.NotifiedLocalPlayer = true
			DSprays.LChatMessage('gui.dsrpays.missing.notification')
		end
	end
end

DSprays.SelectSprayData = dsprays_spray_advanced
DSprays.RequestSpray = dsprays_spray

local spray_bind
local spray_bind_hold = false
local spray_bind_gui, spray_bind_gui2, spray_clicker
local spray_choice, spray_choice_iteration, spray_choice_last, spray_selector_angle
local spray_selector_in = 0

local hold_ruler_button, spray_ruler_bias, spray_ruler_bias_x,
	spray_ruler_bias_mx, spray_ruler_bias_my, spray_ruler_bias_final

local interp_ruler_positions, interp_ruler_positions_shift, interp_ruler_positions_middle = {}, 0, false

local spray_ruler_bias_final_fadein, spray_ruler_bias_final_fadeout,
	spray_ruler_bias_final_size, spray_ruler_bias_final_x,
	spray_ruler_bias_final_y, spray_ruler_bias_final_gdata

local remap_keys = {}
local loop_index, loop_index_choice

for i = 1, 173 do
	local key = input.GetKeyName(i)

	if key then
		remap_keys[key:lower()] = i
	end
end

local function PlayerBindPress(ply, bind, pressed)
	if not pressed then return end
	if bind ~= 'impulse 201' then return end

	local key = remap_keys[input.LookupBinding(bind):lower()]

	if not key then
		spray_bind = nil
		spray_bind_hold = false
		DSprays.WheelGUIActive = false

		if spray_clicker then
			gui.EnableScreenClicker(false)
			spray_clicker = false
		end

		dsprays_spray(true)
		Derma_Message('gui.dsrpays.binding.bad_binding_desc', 'gui.dsrpays.binding.bad_binding', 'gui.misc.ok')
		return true
	end

	if spray_clicker then
		gui.EnableScreenClicker(false)
		spray_clicker = false
	end

	spray_selector_in = 0.8
	spray_selector_angle = nil
	spray_bind = key
	spray_bind_hold = true
	hold_ruler_button = nil
	spray_ruler_bias = nil
	spray_ruler_bias_final = nil
	spray_ruler_bias_x = nil
	DSprays.WheelGUIActive = false
	spray_choice = nil
	spray_choice_last = nil
	spray_bind_gui = SysTime() + 0.25
	spray_bind_gui2 = SysTime() + 0.5

	interp_ruler_positions = {}
	interp_ruler_positions_middle = true
	interp_ruler_positions_shift = 0

	loop_index = {}
	loop_index_choice = {}

	for index, data in SortedPairs(DSprays.SaveData.meta.list) do
		if not data.hide and DSprays.SaveData.data[index] then
			for i, data2 in ipairs(DSprays.SaveData.data[index]) do
				if not data2.hide and DSprays.ValidURL(data2.url) then
					loop_index_choice[table.insert(loop_index, index)] = 0
					break
				end
			end
		end
	end

	return true
end

local BACKGROUND = Color(0, 0, 0, 150)
local BACKGROUND_CHOSEN = Color(85, 197, 255, 50)

surface.DLibCreateFont('dsprays_radial_menu', {
	font = 'PT Sans Caption',
	size = 18,
	extended = true
})

local color_white = color_white

local function GUIMousePressed(code)
	if not spray_bind_hold or not spray_choice then return end
	local getdata = DSprays.SaveData.data[spray_choice]
	if not getdata then return end

	if code == MOUSE_LEFT or code == MOUSE_RIGHT then
		hold_ruler_button = SysTime() + 0.13
		spray_ruler_bias = nil
		spray_ruler_bias_final = nil
		spray_ruler_bias_x = nil
		interp_ruler_positions = {}
		interp_ruler_positions_middle = true
		interp_ruler_positions_shift = 0
		spray_ruler_bias_mx, spray_ruler_bias_my = input.GetCursorPos()
	elseif code == MOUSE_MIDDLE then
		spray_bind_hold = false
		DSprays.WheelGUIActive = false
		DSprays.OpenGUI(spray_choice)
		spray_choice = nil

		if spray_clicker then
			gui.EnableScreenClicker(false)
			spray_clicker = false
		end
	end
end

local function GUIMouseReleased(code)
	if not spray_bind_hold or not spray_choice then return end
	local getdata = DSprays.SaveData.data[spray_choice]
	if not getdata then return end

	local dataindex, main = DSprays.MainFromData(getdata)

	if (code == MOUSE_LEFT or code == MOUSE_RIGHT) and hold_ruler_button and hold_ruler_button < SysTime() then
		if not hold_ruler_button or hold_ruler_button < SysTime() then
			if dataindex and spray_ruler_bias_final then
				getdata[dataindex].main = false
			end

			if spray_ruler_bias_final and getdata[spray_ruler_bias_final] then
				getdata[spray_ruler_bias_final].main = true
			end

			if dataindex or spray_ruler_bias_final then
				timer.Create('DSprays.SaveDataUpdated_' .. spray_choice, 2, 1, DSprays.SaveDataUpdated:Wrap(spray_choice))
			end

			hold_ruler_button = nil
			spray_ruler_bias = nil
			spray_ruler_bias_final = nil
			spray_ruler_bias_x = nil
			input.SetCursorPos(spray_ruler_bias_mx, spray_ruler_bias_my)
		end

		return
	end

	if not dataindex then
		if spray_clicker then
			gui.EnableScreenClicker(false)
			spray_clicker = false
		end

		Derma_Message('gui.dsrpays.binding.gallery_is_empty_desc', 'gui.dsrpays.binding.gallery_is_empty', 'gui.misc.ok')
		return
	end

	if code == MOUSE_RIGHT then
		hold_ruler_button = nil
		local searchindex = dataindex + 1

		if dataindex == #getdata then
			searchindex = 1
		end

		for i = searchindex, #getdata do
			if not getdata[i].hide and DSprays.ValidURL(getdata[i].url) then
				getdata[dataindex].main = false
				getdata[i].main = true

				timer.Create('DSprays.SaveDataUpdated_' .. spray_choice, 2, 1, DSprays.SaveDataUpdated:Wrap(spray_choice))
				return
			end
		end

		for i = 1, dataindex - 1 do
			if not getdata[i].hide and DSprays.ValidURL(getdata[i].url) then
				getdata[dataindex].main = false
				getdata[i].main = true

				timer.Create('DSprays.SaveDataUpdated_' .. spray_choice, 2, 1, DSprays.SaveDataUpdated:Wrap(spray_choice))
				return
			end
		end
	elseif code == MOUSE_LEFT then
		hold_ruler_button = nil
		local searchindex = dataindex - 1

		if dataindex == 1 then
			searchindex = #getdata
		end

		for i = searchindex, 1, -1 do
			if not getdata[i].hide and DSprays.ValidURL(getdata[i].url) then
				getdata[dataindex].main = false
				getdata[i].main = true

				timer.Create('DSprays.SaveDataUpdated_' .. spray_choice, 2, 1, DSprays.SaveDataUpdated:Wrap(spray_choice))
				return
			end
		end

		for i = #getdata, dataindex + 1, -1 do
			if not getdata[i].hide and DSprays.ValidURL(getdata[i].url) then
				getdata[dataindex].main = false
				getdata[i].main = true

				timer.Create('DSprays.SaveDataUpdated_' .. spray_choice, 2, 1, DSprays.SaveDataUpdated:Wrap(spray_choice))
				return
			end
		end
	end
end

local function getDataFrom(getdata, i)
	if #getdata == 0 then return end

	i = i % #getdata

	if i <= 0 then
		i = #getdata + i
	end

	return getdata[i] and not getdata[i].hide and DSprays.ValidURL(getdata[i].url) and getdata[i]
end

local data_with_no_gaps

local function DrawFadeinFunc()
	if not spray_ruler_bias_final_fadein then return end

	local alpha = Cubic(1 - SysTime():progression(spray_ruler_bias_final_fadein, spray_ruler_bias_final_fadeout))

	if alpha <= 0 then
		spray_ruler_bias_final_fadein, spray_ruler_bias_final_fadeout, spray_ruler_bias_final_size, spray_ruler_bias_final_x, spray_ruler_bias_final_y, spray_ruler_bias_final_gdata = nil
		return
	end

	local draw_material, draw_color

	local gdata = spray_ruler_bias_final_gdata
	local size = DSprays.IsRatingCorrect(gdata.rating) and 256 or 16
	local textureData = gdata.spray_type == DSprays.TYPE_DYNAMIC_VIDEO and
		DSprays.VideoAsStaticURLTextureInstant(gdata.url, size, size) or
		DSprays.StaticURLTextureInstant(gdata.url, size, size)

	if textureData and textureData.material_gui then
		draw_material = textureData.material_gui
		draw_color = gdata.color_enable and IsColor(gdata.color) and gdata.color
	else
		draw_material = DSprays.LoadingMaterialGUI
	end

	local size = spray_ruler_bias_final_size * alpha

	if draw_color then
		surface.SetDrawColor(draw_color.r, draw_color.g, draw_color.b, 255 * alpha)
		surface.DrawRect(spray_ruler_bias_final_x - size / 2, spray_ruler_bias_final_y - size / 2, size, size)
	end

	surface.SetMaterial(draw_material)
	surface.SetDrawColor(255, 255, 255, 255 * alpha)
	surface.DrawTexturedRect(spray_ruler_bias_final_x - size / 2, spray_ruler_bias_final_y - size / 2, size, size)
end

local function PostRenderVGUI()
	if not spray_bind or not spray_bind_hold then
		DrawFadeinFunc()
		return
	end

	if not input.IsKeyDown(spray_bind) then
		spray_bind_hold = false
		DSprays.WheelGUIActive = false

		if spray_clicker then
			gui.EnableScreenClicker(false)
			spray_clicker = false
		end

		if spray_bind_gui > SysTime() then
			dsprays_spray(true)
		elseif spray_choice then
			if not spray_ruler_bias then dsprays_spray_advanced(spray_choice) end
			spray_choice = nil
		end

		DrawFadeinFunc()
		return
	end

	if not spray_clicker and spray_bind_gui < SysTime() then
		gui.EnableScreenClicker(true)
		spray_clicker = true
	end

	local total = #loop_index
	if spray_bind_gui > SysTime() or total == 0 then return end

	DSprays.WheelGUIActive = true

	local alpha = math.round(math.progression(SysTime(), spray_bind_gui, spray_bind_gui2) * 255)

	local step = 360 / total
	local W, H = ScrW(), ScrH()
	local size = math.min(W, H) * 0.8
	local size2 = math.min(W, H) * spray_selector_in
	local mW, mH = W / 2 - size / 2, H / 2 - size / 2
	local mW_2, mH_2 = W / 2 - size2 / 2, H / 2 - size2 / 2
	local inLength = size * 0.8
	local inLength2 = (size * 0.2) / 2
	local sizeHalf = size / 2

	local textRadius = size * 0.4
	local pictureRadius = size * 0.3
	local pictureSize = size * 0.15
	local pictureSize2 = size * 0.1
	local pictureSize3 = size * 0.075
	local diff = W * 0.04

	local mouseX, mouseY = input.GetCursorPos()

	surface.SetFont('dsprays_radial_menu')

	DLib.HUDCommons.DrawArcHollow(mW, mH, size, 50, inLength, 360, BACKGROUND)

	spray_selector_in = math.clamp(spray_selector_in + RealFrameTime() * (spray_choice and 0.3 or -0.3), 0.8, 0.85)

	if spray_selector_angle and spray_selector_in > 0.8 then
		DLib.HUDCommons.DrawArcHollow2(mW_2, mH_2, size2, 50, inLength, spray_selector_angle, step, BACKGROUND_CHOSEN:SetAlpha(Cubic(spray_selector_in:progression(0.8, 0.85)) * 50))
	end

	local spray_choice_control = spray_choice
	local index
	local iteration, loop_index_i = 1, 0

	render.PushFilterMag(TEXFILTER.ANISOTROPIC)

	::LOOP_CONTROL::

	iteration = loop_index_i
	loop_index_i = loop_index_i + 1
	index = loop_index[loop_index_i]

	-- skip chosen spray
	if index == spray_choice then
		iteration = loop_index_i
		loop_index_i = loop_index_i + 1
		index = loop_index[loop_index_i]
	end

	if not index then
		-- draw choosen spray over anything else
		if spray_choice_control then
			index = spray_choice_control
			iteration = spray_choice_iteration
			spray_choice_control = nil
			goto LOOP_START
		end

		goto LOOP_END
	end

	::LOOP_START::

	do
		local data = DSprays.SaveData.meta.list[index]
		local getdata = DSprays.SaveData.data[index]

		local degree = (iteration + 0.5) * step
		local x, y = W / 2 - math.cos(degree / 180 * math.pi) * pictureRadius, H / 2 - math.sin(degree / 180 * math.pi) * pictureRadius

		if data.name then
			local text = DLib.I18n.Localize(data.name)
			local w, h = surface.GetTextSize(text)
			draw.SimpleText(text, 'dsprays_radial_menu', x, y - pictureSize / 2 - h - 10, color_white, TEXT_ALIGN_CENTER)
		end

		local dataindex, main = DSprays.MainFromData(getdata)
		local secondLeft, firstLeft, firstRight, secondRight
		local secondLeftC, firstLeftC, firstRightC, secondRightC

		if dataindex then
			for i = dataindex - 1, -#getdata, -1 do
				local gdata = getDataFrom(getdata, i)

				if gdata then
					local size = DSprays.IsRatingCorrect(gdata.rating) and 256 or 16
					local textureData = gdata.spray_type == DSprays.TYPE_DYNAMIC_VIDEO and
						DSprays.VideoAsStaticURLTextureInstant(gdata.url, size, size) or
						DSprays.StaticURLTextureInstant(gdata.url, size, size)

					if textureData and textureData.material_gui then
						if not firstLeft then
							firstLeft = textureData.material_gui
							firstLeftC = gdata.color_enable and IsColor(gdata.color) and gdata.color
						else
							secondLeft = textureData.material_gui
							secondLeftC = gdata.color_enable and IsColor(gdata.color) and gdata.color
							break
						end
					else
						if not firstLeft then
							firstLeft = DSprays.LoadingMaterialGUI
						else
							secondLeft = DSprays.LoadingMaterialGUI
							break
						end
					end
				end
			end

			for i = dataindex + 1, #getdata * 2 do
				local gdata = getDataFrom(getdata, i)
				if gdata then
					local size = DSprays.IsRatingCorrect(gdata.rating) and 256 or 16
					local textureData = gdata.spray_type == DSprays.TYPE_DYNAMIC_VIDEO and
						DSprays.VideoAsStaticURLTextureInstant(gdata.url, size, size) or
						DSprays.StaticURLTextureInstant(gdata.url, size, size)

					if textureData and textureData.material_gui then
						if not firstRight then
							firstRight = textureData.material_gui
							firstRightC = gdata.color_enable and IsColor(gdata.color) and gdata.color
						else
							secondRight = textureData.material_gui
							secondRightC = gdata.color_enable and IsColor(gdata.color) and gdata.color
							break
						end
					else
						if not firstRight then
							firstRight = DSprays.LoadingMaterialGUI
						else
							secondRight = DSprays.LoadingMaterialGUI
							break
						end
					end
				end
			end
		end

		local cubic = Cubic(loop_index_choice[iteration + 1])
		local diff = (diff * (1 + cubic / 2))
		local diffM = (1.4 * (1 + cubic / 3))
		local second = (50 + cubic * 100)
		local first = (150 + cubic * 50)
		local pictureSize = pictureSize * (1 + cubic * 0.4)
		local pictureSize2 = pictureSize2 * (1 + cubic * 0.4)
		local pictureSize3 = pictureSize3 * (1 + cubic * 0.4)
		local mainColor = 255

		if spray_choice_last ~= index and spray_choice_iteration then
			cubic = Cubic(spray_selector_in:progression(0.8, 0.85))
			second = second - 50 * cubic
			first = first - 100 * cubic
			mainColor = mainColor - 100 * cubic
		end

		if secondLeft then
			if secondLeftC then
				surface.SetDrawColor(secondLeftC)
				surface.DrawRect(x - pictureSize3 / 2 - diff * diffM, y - pictureSize3 / 2, pictureSize3, pictureSize3)
			end

			surface.SetDrawColor(second, second, second)
			surface.SetMaterial(secondLeft)

			surface.DrawTexturedRect(x - pictureSize3 / 2 - diff * diffM, y - pictureSize3 / 2, pictureSize3, pictureSize3)
		end

		if firstLeft then
			if firstLeftC then
				surface.SetDrawColor(firstLeftC)
				surface.DrawRect(x - pictureSize2 / 2 - diff, y - pictureSize2 / 2, pictureSize2, pictureSize2)
			end

			surface.SetDrawColor(first, first, first)
			surface.SetMaterial(firstLeft)

			surface.DrawTexturedRect(x - pictureSize2 / 2 - diff, y - pictureSize2 / 2, pictureSize2, pictureSize2)
		end

		if secondRight then
			if secondRightC then
				surface.SetDrawColor(secondRightC)
				surface.DrawRect(x - pictureSize3 / 2 + diff * diffM, y - pictureSize3 / 2, pictureSize3, pictureSize3)
			end

			surface.SetDrawColor(second, second, second)
			surface.SetMaterial(secondRight)

			surface.DrawTexturedRect(x - pictureSize3 / 2 + diff * diffM, y - pictureSize3 / 2, pictureSize3, pictureSize3)
		end

		if firstRight then
			if firstRightC then
				surface.SetDrawColor(firstRightC)
				surface.DrawRect(x - pictureSize2 / 2 + diff, y - pictureSize2 / 2, pictureSize2, pictureSize2)
			end

			surface.SetDrawColor(first, first, first)
			surface.SetMaterial(firstRight)

			surface.DrawTexturedRect(x - pictureSize2 / 2 + diff, y - pictureSize2 / 2, pictureSize2, pictureSize2)
		end

		if main and DSprays.ValidURL(main.url) then
			if main.color_enable and IsColor(main.color) then
				surface.SetDrawColor(main.color)
				surface.DrawRect(x - pictureSize / 2, y - pictureSize / 2, pictureSize, pictureSize)
			end

			local size = DSprays.IsRatingCorrect(main.rating) and 256 or 16
			local textureData = main.spray_type == DSprays.TYPE_DYNAMIC_VIDEO and
				DSprays.VideoAsStaticURLTextureInstant(main.url, size, size) or
				DSprays.StaticURLTextureInstant(main.url, size, size)

			surface.SetDrawColor(mainColor, mainColor, mainColor)
			surface.SetMaterial(textureData and textureData.material_gui or DSprays.LoadingMaterialGUI)

			surface.DrawTexturedRect(x - pictureSize / 2, y - pictureSize / 2, pictureSize, pictureSize)
		end

		if data.random then
			local text = DLib.I18n.Localize('gui.dsrpays.main.labels.randomied')
			local w, h = surface.GetTextSize(text)

			surface.SetDrawColor(0, 0, 0, 200)
			surface.DrawRect(x - 4 - w / 2, y - 2 - h / 2, w + 8, h + 4)

			surface.SetTextColor(0, 0, 0)
			surface.SetTextPos(x + 2 - w / 2, y + 2 - h / 2)
			surface.DrawText(text)

			surface.SetTextColor(255, 255, 255)
			surface.SetTextPos(x - w / 2, y - h / 2)
			surface.DrawText(text)
		end

		goto LOOP_CONTROL
	end

	::LOOP_END::

	render.PopFilterMag()

	if not hold_ruler_button or hold_ruler_button > SysTime() then
		local _spray_choice = spray_choice
		spray_choice = nil

		local distance = math.sqrt(math.pow(W / 2 - mouseX, 2) + math.pow(H / 2 - mouseY, 2))

		if distance > inLength2 and distance < sizeHalf then
			local normalX = (W / 2 - mouseX) / distance
			local angle = math.acos(normalX) * 180 / math.pi

			if H / 2 - mouseY < 0 then
				angle = 360 - angle
			end

			for num2, index in ipairs(loop_index) do
				local num = num2 - 1

				if angle >= num * step and angle < (num + 1) * step then -- lazy to determine index mathematically outside of loop
					if not spray_selector_angle then
						spray_selector_angle = 90 + num * step
					else
						spray_selector_angle = math.ApproachAngle(spray_selector_angle, 90 + num * step, RealFrameTime() * 1000)
					end

					spray_choice = index
					spray_choice_last = index
					spray_choice_iteration = num
					loop_index_choice[num2] = math.min(1, loop_index_choice[num2] + RealFrameTime() * 4)
					break
				end
			end
		end

		if _spray_choice ~= spray_choice then
			data_with_no_gaps = nil

			if spray_choice and DSprays.SaveData.data[spray_choice] then
				data_with_no_gaps = {}
				local _index = 1

				for i, data in ipairs(DSprays.SaveData.data[spray_choice]) do
					if not data.hide and DSprays.ValidURL(data.url) then
						data_with_no_gaps[_index] = data
						_index = _index + 1
					end
				end
			end
		end

		for num2, index in ipairs(loop_index) do
			if index ~= spray_choice then
				loop_index_choice[num2] = math.max(0, loop_index_choice[num2] - RealFrameTime() * 4)
			end
		end
	end

	if hold_ruler_button and hold_ruler_button < SysTime() and spray_choice then
		surface.SetDrawColor(0, 0, 0, 230)
		surface.DrawRect(0, ScrH() / 2 - size / 3, ScrW(), size / 1.5)

		local data = DSprays.SaveData.meta.list[spray_choice]
		local getdata = data_with_no_gaps
		local dataindex, main = DSprays.MainFromData(getdata)

		if not spray_ruler_bias then
			spray_ruler_bias = dataindex
		end

		dataindex = spray_ruler_bias:floor()

		local most_left = ScrW() / 2 - size / 4
		local most_left_middle = most_left
		local most_left_i = 0

		for i = dataindex - 1, -#getdata * 8, -1 do
			local gdata = getDataFrom(getdata, i)

			if gdata then
				most_left_i = most_left_i + 1
				local compute = size * (1 / math.max(1, math.sqrt(most_left_i) * 1.8)) / 2

				most_left = most_left - compute - 4
				if most_left < -400 then break end
			end
		end

		for i = dataindex - most_left_i, dataindex + most_left_i do
			local gdata = getDataFrom(getdata, i)
			local draw_material, draw_color

			if gdata then
				local size = DSprays.IsRatingCorrect(gdata.rating) and 256 or 16
				local textureData = gdata.spray_type == DSprays.TYPE_DYNAMIC_VIDEO and
					DSprays.VideoAsStaticURLTextureInstant(gdata.url, size, size) or
					DSprays.StaticURLTextureInstant(gdata.url, size, size)

				if textureData and textureData.material_gui then
					draw_material = textureData.material_gui
					draw_color = gdata.color_enable and IsColor(gdata.color) and gdata.color
				else
					draw_material = DSprays.LoadingMaterialGUI
				end
			end

			if draw_material then
				local most_left_i2 = math.abs(i - spray_ruler_bias)
				local pictureSize_ = size * (1 / math.max(1, math.sqrt(most_left_i2) * 1.8)) / 2

				local posI = i + interp_ruler_positions_shift

				if not interp_ruler_positions[posI] then
					interp_ruler_positions[posI] = interp_ruler_positions_middle and most_left_middle or most_left
				else
					interp_ruler_positions[posI] = Lerp(RealFrameTime() * 22, interp_ruler_positions[posI], most_left)
				end

				if most_left_i2 < 0.5 then
					spray_ruler_bias_final = i
					spray_ruler_bias_final_fadein = SysTime()
					spray_ruler_bias_final_fadeout = spray_ruler_bias_final_fadein + 0.4
					spray_ruler_bias_final_size = pictureSize_
					spray_ruler_bias_final_x = interp_ruler_positions[posI] + pictureSize_ / 2
					spray_ruler_bias_final_y = H / 2
					spray_ruler_bias_final_gdata = gdata
				end

				if draw_color then
					surface.SetDrawColor(draw_color)
					surface.DrawRect(interp_ruler_positions[posI], H / 2 - pictureSize_ / 2, pictureSize_, pictureSize_)
				end

				surface.SetMaterial(draw_material)
				surface.SetDrawColor(255, 255, 255)
				surface.DrawTexturedRect(interp_ruler_positions[posI], H / 2 - pictureSize_ / 2, pictureSize_, pictureSize_)

				if gdata.label and gdata.label ~= '' then
					draw.SimpleText(gdata.label, 'dsprays_radial_menu', interp_ruler_positions[posI] + pictureSize_ / 2, H / 2 + pictureSize_ / 2, color_white, TEXT_ALIGN_CENTER)
				end

				most_left = most_left + pictureSize_ + 4
			end
		end

		if not spray_ruler_bias_x then
			spray_ruler_bias_x = mouseX
		end

		spray_ruler_bias = (spray_ruler_bias + (mouseX - spray_ruler_bias_x) / size * 4)

		if spray_ruler_bias:abs() >= #getdata then
			interp_ruler_positions_shift = interp_ruler_positions_shift + #getdata * (spray_ruler_bias < 0 and -1 or 1)
			spray_ruler_bias = spray_ruler_bias % #getdata
		end

		if spray_ruler_bias < 1 then
			spray_ruler_bias = spray_ruler_bias + #getdata
			interp_ruler_positions_shift = interp_ruler_positions_shift - #getdata
		end

		spray_ruler_bias_x = mouseX

		if mouseX < W * 0.2 then
			input.SetCursorPos(math.floor(W * 0.75), mouseY)
			spray_ruler_bias_x = math.floor(W * 0.75)
		elseif mouseX > W * 0.8 then
			input.SetCursorPos(math.floor(W * 0.25), mouseY)
			spray_ruler_bias_x = math.floor(W * 0.25)
		end

		interp_ruler_positions_middle = false
	else
		DrawFadeinFunc()
	end
end

concommand.Add('dsprays_spray', dsprays_spray)
hook.Add('PlayerBindPress', 'DSprays Spray Bind Hijack', PlayerBindPress)
hook.Add('PostRenderVGUI', 'DSprays Spray Bind', PostRenderVGUI)
hook.Add('GUIMousePressed', 'DSprays Spray Bind', GUIMousePressed)
hook.Add('GUIMouseReleased', 'DSprays Spray Bind', GUIMouseReleased)
