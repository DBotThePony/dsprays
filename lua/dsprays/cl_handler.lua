
-- Copyright (C) 2020-2021 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

local DSprays = DSprays

local dsprays_rating = CreateConVar('dsprays_rating', '1', {FCVAR_ARCHIVE, FCVAR_USERINFO}, 'Sprays with rating about this will be blurred')
local dsprays_fullbright = CreateConVar('dsprays_fullbright', '0', {FCVAR_ARCHIVE, FCVAR_USERINFO}, 'Use UnlitGeneric material instead of VertexLitGeneric')
local dsprays_texture_size = CreateConVar('dsprays_texture_size', '0', {FCVAR_ARCHIVE}, 'Texture size for in-world sprays. Ranges from -4 (32 (wtf?)) to 2 (2048)')
local dsprays_video_size = CreateConVar('dsprays_video_size', '0', {FCVAR_ARCHIVE}, 'Texture size for in-world video sprays. Ranges from -4 (32 (wtf?)) to 2 (2048)')
local dsprays_hide_everything = CreateConVar('dsprays_hide_everything', '0', {FCVAR_ARCHIVE}, 'Hide all sprays')
local dsprays_hide_unmatching = CreateConVar('dsprays_hide_unmatching', '0', {FCVAR_ARCHIVE}, 'Hide sprays not matching client settings instead of pixeliting them')

DSprays.dsprays_rating = dsprays_rating
DSprays.dsprays_fullbright = dsprays_fullbright
DSprays.dsprays_texture_size = dsprays_texture_size
DSprays.dsprays_video_size = dsprays_video_size
DSprays.dsprays_hide_everything = dsprays_hide_everything
DSprays.dsprays_hide_unmatching = dsprays_hide_unmatching

DSprays.TextureSizeMap = {
	[-4] = 32,
	[-3] = 64,
	[-2] = 128,
	[-1] = 256,
	[0] = 512,
	[1] = 1024,
	[2] = 2048,
}

function DSprays.GetDefaultSize()
	return DSprays.TextureSizeMap[dsprays_texture_size:GetInt():clamp(-4, 2)]
end

function DSprays.GetDefaultSizeVideo()
	return DSprays.TextureSizeMap[dsprays_video_size:GetInt():clamp(-4, 2)]
end

function DSprays.GetCensorSize()
	return 8
end

function DSprays.IsURLHidden(url)
	return cookie.GetString(string.format('dsprays hide %s', url), '0') == '1'
end

function DSprays.HideURL(url)
	return cookie.Set(string.format('dsprays hide %s', url), '1')
end

function DSprays.ShowURL(url)
	return cookie.Delete(string.format('dsprays hide %s', url))
end

function DSprays.IsSteamIDHidden(url)
	return cookie.GetString(string.format('dsprays hide steamid %s', url), '0') == '1'
end

function DSprays.HideSteamID(url)
	return cookie.Set(string.format('dsprays hide steamid %s', url), '1')
end

function DSprays.ShowSteamID(url)
	return cookie.Delete(string.format('dsprays hide steamid %s', url))
end

cvars.AddChangeCallback('dsprays_texture_size', function()
	hook.Run('DSprays_DefaultTextureSizeChanged', DSprays.GetDefaultSize())
	hook.Run('DSprays_SpraySettingsChanged')
end, 'DSprays')

cvars.AddChangeCallback('dsprays_video_size', function()
	hook.Run('DSprays_DefaultVideoSizeChanged', DSprays.GetDefaultSize())
	hook.Run('DSprays_SpraySettingsChanged')
end, 'DSprays')

cvars.AddChangeCallback('dsprays_rating', function()
	hook.Run('DSprays_RatingChanged', dsprays_rating:GetInt():clamp(1, #DSprays.RatingList))
	hook.Run('DSprays_SpraySettingsChanged')
end, 'DSprays')

cvars.AddChangeCallback('dsprays_fullbright', function()
	hook.Run('DSprays_FullbrightChanged', dsprays_fullbright:GetBool())
	hook.Run('DSprays_SpraySettingsChanged')
end, 'DSprays')

cvars.AddChangeCallback('dsprays_hide_everything', function()
	hook.Run('DSprays_HideEverythingChanged', dsprays_hide_everything:GetBool())
	hook.Run('DSprays_SpraySettingsChanged')
end, 'DSprays')

cvars.AddChangeCallback('dsprays_hide_unmatching', function()
	hook.Run('DSprays_HideUnmatchingChanged', dsprays_hide_unmatching:GetBool())
	hook.Run('DSprays_SpraySettingsChanged')
end, 'DSprays')

function DSprays.IsRatingCorrect(ratingIn)
	if not isnumber(ratingIn) then return true end
	return DSprays.dsprays_rating:GetInt():clamp(1, #DSprays.RatingList) >= ratingIn
end
