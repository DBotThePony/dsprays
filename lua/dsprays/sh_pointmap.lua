
-- Copyright (C) 2020-2021 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

local WorldToLocal = WorldToLocal
local LocalToWorld = LocalToWorld
local DSprays = DSprays
local ctorPoint

local POINTMAP = {}

AccessorFunc(POINTMAP, 'parent_entity', 'ParentEntity')

function POINTMAP:ctor(width, height, scale, depth, origin, angles, parentEntity, targetEntity)
	self.map = {}
	self.open = {}
	self.closed = {}
	self.width = width
	self.height = height
	self.scale = scale or 1
	self.depth = depth or 3
	self.origin = origin
	self.angles = angles
	self.normal = angles:Forward()
	self.parent_entity = parentEntity
	self.target_entity = targetEntity

	-- self.trace_mask = MASK_SOLID
	-- self.trace_filter = player.GetAll()

	self.trace_mask = MASK_OPAQUE
	self.trace_filter = {}

	if IsValid(targetEntity) then
		self.trace_mask = MASK_SOLID

		function self.trace_filter(ent)
			return ent == targetEntity
		end
	end

	self.scale_hu_width = scale / (width * 2 + 1)
	self.scale_hu_height = scale / (height * 2 + 1)

	self.size = (width + 1) * height * 2
end

function POINTMAP:Get(x, y)
	return self.map[(self.width + 1) * (y + self.height) * 2 + x + self.width]
end

function POINTMAP:Set(x, y, value)
	local index = (self.width + 1) * (y + self.height) * 2 + x + self.width
	self.map[index] = value
	return index
end

function POINTMAP:GetRoot()
	return self:Get(0, 0)
end

function POINTMAP:GetPos()
	if IsValid(self.parent_entity) then
		return self.parent_entity:GetPos()
	end

	return self.origin
end

function POINTMAP:GetAngles()
	if IsValid(self.parent_entity) then
		return self.parent_entity:GetAngles()
	end

	return self.angles
end

function POINTMAP:GetNormal()
	if IsValid(self.parent_entity) then
		return self.parent_entity:GetAngles():Forward()
	end

	return self.normal
end

local coroutine = coroutine
local coroutine_yield = coroutine.yield

function POINTMAP:Close()
	local open = self.open
	self.open = {}

	local counter = 0
	local closed = self.closed
	local index = #closed

	for i, node in ipairs(open) do
		node:Open()
		closed[index + i] = node

		counter = counter + 1

		if counter >= 50 then
			coroutine_yield()
			counter = 0
		end
	end
end

function POINTMAP:Calculate()
	local counter = 0

	for i, node in ipairs(self.closed) do
		node:SetNeighbours()
		node:CalculateBase()

		counter = counter + 1

		if counter >= 1000 then
			coroutine_yield()
			counter = 0
		end
	end

	local nodes = {self:Get(0, 0)} -- sequential list
	local closed = {} -- processed nodes with hash lookup
	local seen = {} -- hash lookup all nodes seen
	local index = 1 -- index in nodes
	local insertIndex = 2 -- next index in nodes

	repeat
		coroutine_yield()

		local advanceTo = (insertIndex - 1)

		for i = index, advanceTo do
			local node = nodes[i]

			seen[node.index] = true

			local up = node.up and closed[node.up.index] and node.up or nil
			local down = node.down and closed[node.down.index] and node.down or nil
			local left = node.left and closed[node.left.index] and node.left or nil
			local right = node.right and closed[node.right.index] and node.right or nil

			node:CalculateOpen(up, down, left, right)
			closed[node.index] = node

			if node.left and not seen[node.left.index] then
				seen[node.left.index] = true
				nodes[insertIndex] = node.left
				insertIndex = insertIndex + 1
			end

			if node.right and not seen[node.right.index] then
				seen[node.right.index] = true
				nodes[insertIndex] = node.right
				insertIndex = insertIndex + 1
			end

			if node.up and not seen[node.up.index] then
				seen[node.up.index] = true
				nodes[insertIndex] = node.up
				insertIndex = insertIndex + 1
			end

			if node.down and not seen[node.down.index] then
				seen[node.down.index] = true
				nodes[insertIndex] = node.down
				insertIndex = insertIndex + 1
			end

			counter = counter + 1

			if counter >= 100 then
				coroutine_yield()
				counter = 0
			end
		end

		index = advanceTo
	until index == (insertIndex - 1)
end

local math_abs = math.abs

-- i love   -0.000 -0.000 0.000
	--      -0.000 -0.000 0.000
	--      0.000 0.000 0.000
	-- being not equal
local function cmpAngles(a, b)
	if a == b then return true end

	local p1, y1, r1 = a:Unpack()
	local p2, y2, r2 = b:Unpack()

	return math_abs(p1 - p2) < 0.01 and math_abs(y1 - y2) < 0.01 and math_abs(r1 - r2) < 0.01
end

function POINTMAP:IsOnPlane()
	local state, normalAngle2 = self.closed[1]:IsOnPlane()
	if not state then return false end
	local normalAngle

	for i = 2, #self.closed do
		state, normalAngle = self.closed[i]:IsOnPlane()
		if not state then return false end
		if not cmpAngles(normalAngle, normalAngle2) then return false end
	end

	return true, normalAngle2
end

function POINTMAP:BuildMesh(vertex)
	vertex = vertex or {}
	local index = #vertex

	local isIt, normalAngle = self:IsOnPlane()

	if isIt then
		local bottom_left = Vector(0.25, -self.scale / 2, -self.scale / 2)
		local bottom_right = Vector(0.25, self.scale / 2, -self.scale / 2)
		local top_left = Vector(0.25, -self.scale / 2, self.scale / 2)
		local top_right  = Vector(0.25, self.scale / 2, self.scale / 2)

		top_left:Rotate(normalAngle)
		top_right:Rotate(normalAngle)
		bottom_left:Rotate(normalAngle)
		bottom_right:Rotate(normalAngle)

		local normal = normalAngle:Forward()

		-- triangle 1
		vertex[index + 1] = {
			pos = top_right,
			color = color_white,
			u = 1,
			v = 0,
			normal = normal,
		}

		vertex[index + 2] = {
			pos = bottom_left,
			color = color_white,
			u = 0,
			v = 1,
			normal = normal,
		}

		vertex[index + 3] = {
			pos = top_left,
			color = color_white,
			u = 0,
			v = 0,
			normal = normal,
		}

		-- triangle 2
		vertex[index + 4] = {
			pos = top_right,
			color = color_white,
			u = 1,
			v = 0,
			normal = normal,
		}

		vertex[index + 5] = {
			pos = bottom_right,
			color = color_white,
			u = 1,
			v = 1,
			normal = normal,
		}

		vertex[index + 6] = {
			pos = bottom_left,
			color = color_white,
			u = 0,
			v = 1,
			normal = normal,
		}
	else
		local counter = 0

		for i, node in ipairs(self.closed) do
			vertex, index = node:BuildMesh(vertex, index)

			counter = counter + 1

			if counter >= 200 then
				coroutine_yield()
				counter = 0
			end
		end
	end

	return vertex
end

local POINT = {}
local developer = ConVar('developer')

function POINT:ctor(map, origin, angles, x, y)
	self.map = map
	self.x = x
	self.y = y
	self.xlimit = map.width
	self.ylimit = map.height
	self.origin = origin
	self.angles = angles

	--[[if developer:GetBool() then
		local Wpos, Wang = self:GetWorldSpacePos()
		debugoverlay.Cross(Wpos, 1, 4, color_cyan, true)

		debugoverlay.Line(Wpos, Wpos - Wang:Right() * 5, 4, color_red, true)
		debugoverlay.Line(Wpos, Wpos + Wang:Up() * 5, 4, color_green, true)
	end]]

	map.open[#map.open + 1] = self

	self.index = map:Set(x, y, self)
end

function POINT:GetWorldSpacePos()
	return LocalToWorld(self.origin, self.angles, self.map:GetPos(), self.map:GetAngles())
end

local assert = assert

function POINT:SetNeighbours()
	self.up = self.map:Get(self.x, self.y + 1)
	self.down = self.map:Get(self.x, self.y - 1)
	self.left = self.map:Get(self.x - 1, self.y)
	self.right = self.map:Get(self.x + 1, self.y)

	if self.up then
		assert(self.up.x == self.x, 'self.up.x != self.x')
		assert(self.up.y == (self.y + 1), 'self.up.y != (self.y + 1)')
	end

	if self.down then
		assert(self.down.x == self.x, 'self.down.x != self.x')
		assert(self.down.y == (self.y - 1), 'self.down.y != (self.y - 1)')
	end

	if self.left then
		assert(self.left.x == (self.x - 1), 'self.left.x != (self.x - 1)')
		assert(self.left.y == self.y, 'self.left.y != self.y')
	end

	if self.right then
		assert(self.right.x == (self.x + 1), 'self.right.x != (self.x + 1)')
		assert(self.right.y == self.y, 'self.right.y != self.y')
	end
end

local Vector = Vector
local util_TraceLine = util.TraceLine
local vector_down = Vector(0, 0, -1)
local vector_up = Vector(0, 0, 1)

function POINT:Expand(xDelta, yDelta, expandVector, expandMult, oExpandMult, Wpos, Wang, Wnormal, Wright, Wleft, Wup)
	local trStart1 = Vector(Wpos)
	trStart1:Add(Wnormal)

	local expandWithMult = expandVector * expandMult
	local oExpandWithMult = expandVector * oExpandMult

	-- convex advance
	local trAdvance = util_TraceLine({
		start = trStart1,
		endpos = trStart1 + oExpandWithMult,
		--mask = MASK_OPAQUE,
		filter = self.map.trace_filter,
		mask = self.map.trace_mask
	})

	local tr, realAngle

	-- bend
	if trAdvance.Hit then
		tr = trAdvance

		local translatePos, translateAng = WorldToLocal(tr.HitPos, tr.HitNormal:Angle(), self.map:GetPos(), self.map:GetAngles())
		local dot = translateAng:Forward():Dot(vector_up)

		if dot >= 0.9 then
			realAngle = Angle(Wang)
			realAngle:RotateAroundAxis(Wright, 90)
			tr.HitPos:Add(realAngle:Up() * 1)
		elseif dot <= 0.9 then
			realAngle = Angle(Wang)
			realAngle:RotateAroundAxis(Wleft, 90)
			tr.HitPos:Add(realAngle:Up() * -1)
		else
			realAngle = tr.HitNormal:Angle()
			tr.HitPos:Add(realAngle:Right() * (tr.HitNormal:Dot(Wleft) * expandMult))
		end
	else
		-- point detirminition as flat surafce
		tr = util_TraceLine({
			start = Wpos + Wnormal * self.map.depth + expandWithMult,
			endpos = Wpos - Wnormal * (self.map.depth * 2) + expandWithMult,
			--mask = MASK_OPAQUE,
			filter = self.map.trace_filter,
			mask = self.map.trace_mask
		})

		-- let's try to do it as concave
		if not tr.Hit then
			local trStart2 = Vector(Wpos)
			trStart2:Sub(Wnormal)

			tr = util_TraceLine({
				start = trStart2 + expandVector,
				endpos = trStart2 - expandVector * 2,
				--mask = MASK_OPAQUE,
				filter = self.map.trace_filter,
				mask = self.map.trace_mask
			})

			local translatePos, translateAng = WorldToLocal(tr.HitPos, tr.HitNormal:Angle(), self.map:GetPos(), self.map:GetAngles())
			local dot = translateAng:Forward():Dot(vector_up)

			if dot >= 0.9 then
				realAngle = Angle(Wang)
				realAngle:RotateAroundAxis(Wright, 90)
				tr.HitPos:Add(realAngle:Up() * 1)
			elseif dot <= -0.9 then
				realAngle = Angle(Wang)
				realAngle:RotateAroundAxis(Wleft, 90)
				tr.HitPos:Add(realAngle:Up() * -1)
			else
				realAngle = tr.HitNormal:Angle()

				dot = tr.HitNormal:Dot(self.map:GetNormal())

				if dot <= -0.9 then
					realAngle:RotateAroundAxis(tr.HitNormal, 180)
					tr.HitPos:Sub(realAngle:Up() * expandMult)
				end
			end
		else
			realAngle = Angle(Wang)
			local mdot = Wup:Dot(tr.HitNormal)

			if mdot:abs() > 0.1 then
				realAngle:RotateAroundAxis(mdot < 0 and Wleft or Wright, tr.HitNormal:Dot(Wnormal):clamp(-1, 1):acos():deg())
			else
				mdot = Wright:Dot(tr.HitNormal)
				realAngle:RotateAroundAxis(Wup, tr.HitNormal:Dot(Wnormal):clamp(-1, 1):acos():deg() * (mdot > 0 and -1 or 1))
			end
		end
	end

	if tr.Hit then
		local x, y = self.x + xDelta, self.y + yDelta
		local Lpos, Lang = WorldToLocal(tr.HitPos, realAngle, self.map:GetPos(), self.map:GetAngles())

		ctorPoint(self.map, Lpos, Lang, x, y, haveToDebug)
	else
		debugoverlay.Cross(tr.HitPos, 1, 4, color_red, true)
	end
end

function POINT:Open()
	local Wpos, Wang = self:GetWorldSpacePos()
	local Wnormal = Wang:Forward()
	local Wright = Wang:Right()
	local Wleft = Wright * -1
	local Wup = Wang:Up()

	local scale = self.map.scale_hu_height
	local oScale = math.max(2, scale)

	-- up
	if math.abs(self.y + 1) <= self.ylimit and not self.map:Get(self.x, self.y + 1) then
		self:Expand(0, 1, Wang:Up(), scale, oScale, Wpos, Wang, Wnormal, Wright, Wleft, Wup)
	end

	-- down
	if math.abs(self.y - 1) <= self.ylimit and not self.map:Get(self.x, self.y - 1) then
		self:Expand(0, -1, -Wang:Up(), scale, oScale, Wpos, Wang, Wnormal, Wright, Wleft, Wup)
	end

	-- left
	if math.abs(self.x - 1) <= self.xlimit and not self.map:Get(self.x - 1, self.y) then
		self:Expand(-1, 0, Wang:Right(), scale, oScale, Wpos, Wang, Wnormal, Wright, Wleft, Wup)
	end

	-- right
	if math.abs(self.x + 1) <= self.xlimit and not self.map:Get(self.x + 1, self.y) then
		self:Expand(1, 0, -Wang:Right(), scale, oScale, Wpos, Wang, Wnormal, Wright, Wleft, Wup)
	end
end

function POINT:CalculateBase()
	local init = Vector(self.origin)
	init:Add(self.angles:Forward())

	local up = self.angles:Up()
	up:Mul(self.map.scale_hu_height / 2)

	local right = self.angles:Right()
	right:Mul(-self.map.scale_hu_width / 2)

	local base_top_left     = Vector(init)
	local base_top_right    = Vector(init)
	local base_bottom_left  = Vector(init)
	local base_bottom_right = Vector(init)

	self.base_top_left      = base_top_left
	self.base_top_right     = base_top_right
	self.base_bottom_left   = base_bottom_left
	self.base_bottom_right  = base_bottom_right

	base_top_left:Add(up)
	base_top_left:Sub(right)

	base_top_right:Add(up)
	base_top_right:Add(right)

	base_bottom_left:Sub(up)
	base_bottom_left:Sub(right)

	base_bottom_right:Sub(up)
	base_bottom_right:Add(right)

	local stepU, stepV = 0.5 / (self.map.width * 2 + 1), 0.5 / (self.map.height * 2 + 1)

	local u = (self.map.width + self.x + 0.5) / (self.map.width * 2 + 1)
	local v = 1 - (self.map.height + self.y + 0.5) / (self.map.height * 2 + 1)

	self.u1 = math.clamp(u - stepU, 0, 1)
	self.v1 = math.clamp(v - stepV, 0, 1)

	self.u2 = math.clamp(u + stepU, 0, 1)
	self.v2 = math.clamp(v - stepV, 0, 1)

	self.u3 = math.clamp(u - stepU, 0, 1)
	self.v3 = math.clamp(v + stepV, 0, 1)

	self.u4 = math.clamp(u + stepU, 0, 1)
	self.v4 = math.clamp(v + stepV, 0, 1)
end

local LerpVector = LerpVector

function POINT:CalculateOpen(up, down, left, right)
	if down and left then
		self.bottom_left = LerpVector(0.5, down.top_left, left.bottom_right)
		self.bottom_left_angle = LerpAngle(0.5, down.top_left_angle, left.bottom_right_angle)
	elseif down then
		self.bottom_left = down.top_left
		self.bottom_left_angle = down.top_left_angle
	elseif left then
		self.bottom_left = left.bottom_right
		self.bottom_left_angle = left.bottom_right_angle
	end

	if down and right then
		self.bottom_right = LerpVector(0.5, down.top_right, right.bottom_left)
		self.bottom_right_angle = LerpAngle(0.5, down.top_right_angle, right.bottom_left_angle)
	elseif down then
		self.bottom_right = down.top_right
		self.bottom_right_angle = down.top_right_angle
	elseif right then
		self.bottom_right = right.bottom_left
		self.bottom_right_angle = right.bottom_left_angle
	end

	if up and left then
		self.top_left = LerpVector(0.5, up.bottom_left, left.top_right)
		self.top_left_angle = LerpAngle(0.5, up.bottom_left_angle, left.top_right_angle)
	elseif up then
		self.top_left = up.bottom_left
		self.top_left_angle = up.bottom_left_angle
	elseif left then
		self.top_left = left.top_right
		self.top_left_angle = left.top_right_angle
	end

	if up and right then
		self.top_right = LerpVector(0.5, up.bottom_right, right.top_left)
		self.top_right_angle = LerpAngle(0.5, up.bottom_right_angle, right.top_left_angle)
	elseif up then
		self.top_right = up.bottom_right
		self.top_right_angle = up.bottom_right_angle
	elseif right then
		self.top_right = right.top_left
		self.top_right_angle = right.top_left_angle
	end

	if not self.top_left then
		self.top_left = self.base_top_left
		-- self.top_left, self.top_left_angle = WorldToLocal(self.base_top_left, self.tr.HitNormal:Angle(), self.map.origin, self.map.angles)
		self.top_left_angle = self.angles
	end

	if not self.top_right then
		self.top_right = self.base_top_right
		-- self.top_right, self.top_right_angle = WorldToLocal(self.base_top_right, self.tr.HitNormal:Angle(), self.map.origin, self.map.angles)
		self.top_right_angle = self.angles
	end

	if not self.bottom_left then
		self.bottom_left = self.base_bottom_left
		-- self.bottom_left, self.bottom_left_angle = WorldToLocal(self.base_bottom_left, self.tr.HitNormal:Angle(), self.map.origin, self.map.angles)
		self.bottom_left_angle = self.angles
	end

	if not self.bottom_right then
		self.bottom_right = self.base_bottom_right
		-- self.bottom_right, self.bottom_right_angle = WorldToLocal(self.base_bottom_right, self.tr.HitNormal:Angle(), self.map.origin, self.map.angles)
		self.bottom_right_angle = self.angles
	end
end

-- whenever entire point is on single plane
function POINT:IsOnPlane()
	local isIt = cmpAngles(self.top_left_angle, self.top_right_angle) and
		cmpAngles(self.bottom_left_angle, self.bottom_right_angle) and
		cmpAngles(self.top_left_angle, self.bottom_left_angle)

	return isIt, self.top_left_angle
end

local color_white = Color()

function POINT:BuildMesh(vertex, index)
	-- triangle 1
	vertex[index + 1] = {
		pos = self.top_right or self.base_top_right,
		color = color_white,
		u = self.u2,
		v = self.v2,
		normal = self.top_right_angle and self.top_right_angle:Forward(),
	}

	vertex[index + 2] = {
		pos = self.bottom_left or self.base_bottom_left,
		color = color_white,
		u = self.u3,
		v = self.v3,
		normal = self.bottom_left_angle and self.bottom_left_angle:Forward(),
	}

	vertex[index + 3] = {
		pos = self.top_left or self.base_top_left,
		color = color_white,
		u = self.u1,
		v = self.v1,
		normal = self.top_left_angle and self.top_left_angle:Forward(),
	}

	-- triangle 2
	vertex[index + 4] = {
		pos = self.top_right or self.base_top_right,
		color = color_white,
		u = self.u2,
		v = self.v2,
		normal = self.top_right_angle and self.top_right_angle:Forward(),
	}

	vertex[index + 5] = {
		pos = self.bottom_right or self.base_bottom_right,
		color = color_white,
		u = self.u4,
		v = self.v4,
		normal = self.bottom_right_angle and self.bottom_right_angle:Forward(),
	}

	vertex[index + 6] = {
		pos = self.bottom_left or self.base_bottom_left,
		color = color_white,
		u = self.u3,
		v = self.v3,
		normal = self.bottom_left_angle and self.bottom_left_angle:Forward(),
	}

	return vertex, index + 6
end

DSprays.PointMap = DLib.CreateMoonClass('PointMap', POINTMAP)
DSprays.Point = DLib.CreateMoonClass('Point', POINT)

ctorPoint = DSprays.Point
